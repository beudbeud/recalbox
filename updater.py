#!/usr/bin/python3
import os
import sys
import re
import requests
import json
from datetime import datetime


def get_date(site, org, project):
    if site == "github":
        try:
            x = requests.get("https://api.github.com/repos/%s/%s/git/commits/%s" % (org, project, version))
            req = json.loads(x.text)
            date = datetime.strptime(req["committer"]["date"], "%Y-%m-%dT%H:%M:%SZ").strftime("%Y/%m/%d")
        except:
            date = "NEED FIX!!"
            print("An exception occurred") 
    elif site == "gitlab":
        try:
            x = requests.get("https://gitlab.com/api/v4/projects/%s%2F%s/repository/commits/%s" % (org, project, version))
            req = json.loads(x.text)
            date = datetime.strptime(req["committed_date"], "%Y-%m-%dT%H:%M:%S").strftime("%Y/%m/%d")
        except:
            date = "NEED FIX!!"
            print("An exception occurred") 
    return date


mk = sys.argv[1]
package = mk.split('/')[1]
update_changelog = True

with open(mk, "r+") as f:
    data = f.read()
    version = re.search(r".*_VERSION = (.*)", data).group(1)
    if re.search(r".*_SITE = \$\(call .*", data):
        result = re.search(r".*_SITE = \$\(call ([a-z]*),([a-zA-Z0-9_-]*),([a-zA-Z0-9_-]*).*", data)
        site = result.group(1)
        org = result.group(2)
        project = result.group(3)
    else:
        result = re.search(r".*_SITE = https\:\/\/(\w+).com\/([a-zA-Z0-9_\-]*)\/([a-zA-Z0-9_\-]*)(?:\.git)?", data)
        site = result.group(1)
        org = result.group(2)
        project = result.group(3)
    date = get_date(site, org, project)
    new_mk = re.sub(r"# .*ommit .* (\d+.\d+.\d+)(.*)", r"# Commit of %s\g<2>" % (date), data)
    f.seek(0)
    f.write(new_mk)
    f.truncate()

with open("RELEASE-NOTES.md", "r+") as f:
    data = f.read()
    bumps = re.search(r"### Bumps((?:\n-.*)*)", data).group(1)
    bumps_list = bumps.splitlines()
    for i in bumps_list:
        pq = re.sub(r"- Bump (.*)", r"\g<1>", i)
        if pq == package:
            update_changelog = False
    if update_changelog:
        new_rn = re.sub(r"### Bumps((?:\n-.*)*)", r"### Bumps\g<1>\n- Bump %s" % (package), data, 1)
        f.seek(0)
        f.write(new_rn)
        f.truncate()
    else:
        f.close
