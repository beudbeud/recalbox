//
// Created by bkg2k on 17/10/24.
//

/*
 * Disable switch all enum values warning as in this source code
 * we have big enums in several methods with only a few values really used
 * Code consistency is enforced at runtime instead
 */
#pragma GCC diagnostic ignored "-Wswitch-enum"

#include <algorithm>
#include <guis/menus/MenuProvider.h>
#include <RootFolders.h>
#include <Upgrade.h>
#include <hardware/devices/storage/StorageDevices.h>
#include <MainRunner.h>
#include "scraping/ScraperFactory.h"
#include "guis/GuiMsgBox.h"
#include "guis/GuiNetPlay.h"
#include "guis/GuiBiosScan.h"
#include "guis/GuiScraperRun.h"
#include "LibretroRatio.h"
#include "MenuTools.h"
#include "MenuPads.h"
#include "guis/menus/vault/MenuCRT.h"
#include "guis/menus/vault/MenuUserInterface.h"
#include "guis/menus/vault/MenuArcade.h"
#include "MenuTate.h"
#include "MenuSound.h"
#include "guis/menus/vault/MenuNetwork.h"
#include "MenuDownloadContents.h"
#include "guis/menus/modaltasks/MenuModalHashRom.h"
#include "guis/menus/modaltasks/MenuModalTaskWps.h"
#include "guis/menus/modaltasks/MenuModalTaskWifi.h"
#include "guis/menus/vault/MenuThemeOptions.h"
#include "EmulationStation.h"
#include "systems/arcade/ArcadeVirtualSystems.h"
#include "guis/menus/modaltasks/MenuModalInitDevice.h"
#include "guis/menus/modaltasks/MenuModalFactoryReset.h"
#include "hardware/RPiEepromUpdater.h"
#include <systems/SystemManager.h>
#include <guis/menus/base/ItemAction.h>
#include <guis/menus/base/ItemSwitch.h>
#include <guis/menus/base/ItemSelector.h>
#include <guis/menus/base/ItemEditable.h>
#include <guis/menus/base/ItemSubMenu.h>

void MenuProvider::DeserializeMenus()
{
  const Path xmlPath = RootFolders::TemplateRootFolder / "system/.emulationstation/menu.xml";
  String xml;
  if (!SecuredFile::LoadSecuredFile(false, xmlPath, Path::Empty, xml, "DefMenu", true))
    return;

  XmlDocument gameList;
  XmlResult result = gameList.load_string(xml.data());
  if (!result)
  {
    { LOG(LogFatal) << "[MenuProvider] Cannot parse " << xmlPath.ToString() << " file!"; }
    return;
  }

  XmlNode menus = gameList.child("menus");
  if (menus != nullptr)
    for (const XmlNode node: menus.children())
    {
      String name = node.name();
      if (name == "menu") DeserializeMenu("root", node, nullptr);
      else { LOG(LogFatal) << "[MenuProvider] Unknown menu node \"" << name << "\", skipping."; }
    }
  else { LOG(LogFatal) << "[MenuProvider] No menu definition in \"" << xmlPath; }

  // Post-processing
  CheckMenuReferences();

  { LOG(LogInfo) << "[MenuProvider] " << mMenuDefinitions.size() << " menu definitions loaded."; }
}

void MenuProvider::DeserializeMenu(const String& parentId, const XmlNode& menu, std::vector<ItemDefinition>* parentItems)
{
  // Syntax check
  static Array<const char*> properties { "id", "caption", "altCaptionIf", "animated", "help" , "grayedHelp", "if", "icon" };
  CheckProperties(parentId, properties, menu);

  String id = Xml::AttributeAsString(menu, "id", "");
  LOG(LogDebug) << "[MenuProvider] Deserialize menu " << id;
  if (id.empty()) { LOG(LogError) << "[MenuProvider] No id provided ! Menu skipped"; return; }
  MenuContainerType type = MenuConverters::MenuFromString(id);
  if (mMenuDefinitions.contains(type)) { LOG(LogFatal) << "[MenuProvider] Menu '" << id << "' redefined ! Menu skipped"; }
  String caption = Xml::AttributeAsString(menu, "caption", "<NO TITLE>");
  if (caption.empty()) { LOG(LogError) << "[MenuProvider] No title provided for menu id: " << id; }
  String altCaptionIf = Xml::AttributeAsString(menu, "altCaptionIf", "");
  bool animated = Xml::AttributeAsBool(menu, "animated", false);
  String help = Xml::AttributeAsString(menu, "help", "");
  String grayedHelp = Xml::AttributeAsString(menu, "grayedHelp", "");
  String condition = Xml::AttributeAsString(menu, "if", "");
  String iconName = Xml::AttributeAsString(menu, "icon", "");

  std::vector<ItemDefinition> items;
  for (const XmlNode node: menu.children())
  {
    String name = node.name();
    // Deserialize inner submenu
    if (name == "menu") DeserializeMenu(id, node, &items);
    // Menu reference
    else if (name == "menuref") DeserializeMenuRef(id, items, node);
    // Item
    else if (name == "item") DeserializeItem(id, items, node);
    // Header
    else if (name == "header") DeserializeHeader(id, items, node);
    // Error
    else { LOG(LogFatal) << "[MenuProvider] Unknown menu node " << name; }
  }

  MenuThemeData::MenuIcons::Type icon = MenuThemeData::MenuIcons::IconFromString(iconName);
  mMenuDefinitions.insert(type, MenuDefinition(type, icon, caption, altCaptionIf, MenuDefinition::Footer::Recalbox, std::move(items),
                                               help, grayedHelp, condition, animated));
  if (parentItems != nullptr)
    parentItems->push_back(ItemDefinition(type, icon, caption, altCaptionIf, help, grayedHelp, condition));
}

void MenuProvider::DeserializeMenuRef(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& menuRef)
{
  // Syntax check
  static Array<const char*> properties { "id", "icon", "caption", "altCaptionIf", "if", "help", "grayedHelp" };
  CheckProperties(parentId, properties, menuRef);

  // Get & check attributes
  String ref = Xml::AttributeAsString(menuRef, "id", "");
  LOG(LogDebug) << "[MenuProvider] Deserialize menuref " << ref;
  if (ref.empty()) { LOG(LogFatal) << "[MenuProvider] Menu reference missing or empty in parent menu " << parentId << ". Submenu skipped"; }
  MenuContainerType menu = MenuConverters::MenuFromString(ref);
  String iconName = Xml::AttributeAsString(menuRef, "icon", "");
  MenuThemeData::MenuIcons::Type icon = MenuThemeData::MenuIcons::IconFromString(iconName);

  // Lookup existing menu definitiuon
  MenuDefinition* def = mMenuDefinitions.try_get(menu);
  if (def == nullptr) { LOG(LogFatal) << "[MenuProvider] Menu " << ref << " must be fully defined before its first reference!"; __builtin_unreachable(); }

  // Override
  String caption = Xml::AttributeAsString(menuRef, "caption", def->mCaption);
  String altCaptionIf = Xml::AttributeAsString(menuRef, "altCaptionIf", def->mCaptionIf);
  String help = Xml::AttributeAsString(menuRef, "help", def->mHelp);
  String grayedHelp = Xml::AttributeAsString(menuRef, "grayedHelp", def->mUnselectableHelp);
  String condition = Xml::AttributeAsString(menuRef, "if", def->mCondition);

  // Store
  items.push_back(ItemDefinition(menu, icon, caption, altCaptionIf, help, grayedHelp, condition));
}

void MenuProvider::DeserializeItem(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& item)
{
  // Syntax check
  static Array<const char*> properties { "id", "caption", "altCaptionIf", "help" , "grayedHelp", "if", "icon", "relaunch", "reboot", "bootConf" };
  CheckProperties(parentId, properties, item);

  // Get & check attributes
  String typeString = Xml::AttributeAsString(item, "id", "");
  LOG(LogDebug) << "[MenuProvider] Deserialize item " << typeString;
  if (typeString.empty()) { LOG(LogFatal) << "[MenuProvider] Item identifier missing or empty in parent menu " << parentId << ". Item skipped"; }
  MenuItemType type = MenuConverters::ItemFromString(typeString);
  if (type == MenuItemType::_Error_) { LOG(LogFatal) << "[MenuProvider] Unknown Item identifier " << typeString << " in parent menu " << parentId << ". Item skipped"; }
  String caption = Xml::AttributeAsString(item, "caption", "<MISSING CAPTION>");
  if (caption.empty()) { LOG(LogFatal) << "[MenuProvider] Item missing caption in parent menu " << parentId << '.'; }
  String altCaptionIf = Xml::AttributeAsString(item, "altCaptionIf", "");
  String help = Xml::AttributeAsString(item, "help", "");
  String grayedHelp = Xml::AttributeAsString(item, "grayedHelp", "");
  String condition = Xml::AttributeAsString(item, "if", "");
  String iconName = Xml::AttributeAsString(item, "icon", "");
  bool relaunch = Xml::AttributeAsBool(item, "relaunch", false);
  bool reboot = Xml::AttributeAsBool(item, "reboot", false);
  bool bootConf = Xml::AttributeAsBool(item, "bootConf", false);

  // Store
  MenuThemeData::MenuIcons::Type icon = MenuThemeData::MenuIcons::IconFromString(iconName);
  items.push_back(ItemDefinition(type, icon, caption, altCaptionIf, help, grayedHelp, condition, relaunch, reboot, bootConf));
}

void MenuProvider::DeserializeHeader(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& header)
{
  // Syntax check
  static Array<const char*> properties { "caption" };
  CheckProperties(parentId, properties, header);

  String caption = Xml::AttributeAsString(header, "caption", "<MISSING CAPTION>");
  if (caption.empty()) { LOG(LogFatal) << "[MenuProvider] Header missing caption in parent menu " << parentId << '.'; }

  // Store
  items.push_back(ItemDefinition(caption));
}

void MenuProvider::CheckMenuReferences()
{
  for(const auto& menu : mMenuDefinitions)
    for(const ItemDefinition& items : menu.second.mItems)
      if (items.Category() == ItemDefinition::ItemCategory::SubMenu)
        if (!mMenuDefinitions.contains(items.MenuType()))
        { LOG(LogFatal) << "[MenuProvider] Unknown reference to menu '" << (int)items.MenuType() << "' in menu captioned '" << menu.second.mCaption << '\''; }
}

void MenuProvider::BuildMenu(MenuContainerType identifier, const InheritableContext& context, const ItemBase* itemParent)
{
  MenuDefinition* menu = mMenuDefinitions.try_get(identifier);
  if (menu == nullptr) { LOG(LogError) << "[MenuProvider] Unknown menu identifier '" << (int)identifier << '\''; return; }
  BuildMenu(*menu, context, itemParent);
}

void MenuProvider::BuildMenu(const MenuDefinition& definition, const InheritableContext& context, const ItemBase* itemParent)
{
  // Build menu GUI
  MenuBuilder* gui = new MenuBuilder(mWindow, context, *this, definition, mResolver, itemParent);
  mWindow.pushGui(gui);
}

void MenuProvider::CheckProperties(const String& parentId, const Array<const char*>& allowedProperties, const XmlNode& node)
{
  for(const XmlAttribute& attribute : node.attributes())
  {
    bool found = false;
    for(const char* allowed : allowedProperties)
      if (strcmp(allowed, attribute.name()) == 0) { found = true; break; }
    if (!found)
    { LOG(LogFatal) << "[MenuProvider] Unknown attribute " << attribute.name() << " in item " << node.name() << " of parent menu id " << parentId; }
  }
}

/*
 * ================================================================================================================
 * SUB MENU HANDLERS
 * ================================================================================================================
 */

void MenuProvider::SubMenuSelected(const ItemSubMenu& item, int id)
{
  MenuContainerType menuType = (MenuContainerType)id;

  // ------- TEMP CODE BEGIN
  if (menuType == MenuContainerType::Controllers) { mWindow.pushGui(new MenuPads(mWindow)); return; }
  if (menuType == MenuContainerType::Crt        ) { mWindow.pushGui(new MenuCRT(mWindow, mSystemManager, Board::Instance().CrtBoard().GetCrtAdapter() == CrtAdapterType::RGBJamma ? _("JAMMA SETTINGS") : _("CRT SETTINGS"))); return; }
  if (menuType == MenuContainerType::Tate       ) { mWindow.pushGui(new MenuTate(mWindow, mSystemManager)); return; }
  //if (menuType == MenuContainerType::Advanced   ) { mWindow.pushGui(new MenuAdvancedSettings(mWindow, mSystemManager)); return; }
  if (menuType == MenuContainerType::Sound      ) { mWindow.pushGui(new MenuSound(mWindow)); return; }
  if (menuType == MenuContainerType::Download   ) { mWindow.pushGui(new MenuDownloadContents(mWindow, mSystemManager)); return; }
  // ------- TEMP CODE ENDS

  BuildMenu(menuType, item.Context(), &item);
}

bool MenuProvider::IsMenuUnselectable(MenuContainerType identifier)
{
  (void)identifier;
  return false;
}

/*
 * ================================================================================================================
 * SELECTOR ITEM GETTERS
 * ================================================================================================================
 */

SelectorEntry<StorageDevices::Device>::List MenuProvider::GetStorageEntries(int& currentIndex)
{
  SelectorEntry<StorageDevices::Device>::List list;
  int index = 0;
  for(const StorageDevices::Device& device : mStorageDevices.GetShareDevices())
  {
    String display = device.DisplayName;
    if (device.Size != 0)
      display.Append(" (").Append(_("Free"))
             .Append(' ').Append(device.HumanFree())
             .Append('/').Append(device.HumanSize()).Append(')');
    list.push_back({ display, device, device.Current });
    if (device.Current) currentIndex = index;
    index++;
  }
  return list;
}

SelectorEntry<String>::List MenuProvider::GetCultureEntries()
{
  return SelectorEntry<String>::List
    ({
       { "اللغة العربية" , "ar_YE" },
       { "CATALÀ"        , "ca_ES" },
       { "čeština"       , "cs_CZ" },
       { "DEUTSCH"       , "de_DE" },
       { "DEUTSCH (CH)"  , "de_CH" },
       { "ελληνικά"      , "el_GR" },
       { "ENGLISH"       , "en_US" },
       { "ENGLISH (UK)"  , "en_GB" },
       { "ESPAÑOL"       , "es_ES" },
       { "EUSKARA"       , "eu_ES" },
       { "PERSIAN"       , "fa_IR" },
       { "FRANÇAIS"      , "fr_FR" },
       { "GALICIAN"      , "gl_ES" },
       { "MAGYAR"        , "hu_HU" },
       { "ITALIANO"      , "it_IT" },
       { "日本語"         , "ja_JP" },
       { "한국어"         , "ko_KR" },
       { "Lëtzebuergesch", "lb_LU" },
       { "latviešu"      , "lv_LV" },
       { "BOKMAL"        , "nb_NO" },
       { "NEDERLANDS"    , "nl_NL" },
       { "NORSK"         , "nn_NO" },
       { "POLSKI"        , "pl_PL" },
       { "PORTUGUES"     , "pt_BR" },
       { "ROMANIAN"      , "ro_RO" },
       { "Русский"       , "ru_RU" },
       { "SLOVAK"        , "sk_SK" },
       { "SVENSKA"       , "sv_SE" },
       { "TÜRKÇE"        , "tr_TR" },
       { "UKRAINIAN"     , "uk_UA" },
       { "简体中文"       , "zh_CN" },
       { "正體中文"       , "zh_TW" },
     });
}

SelectorEntry<String>::List MenuProvider::GetKeyboardEntries()
{
  return SelectorEntry<String>::List
    ({
       { "DE (German QWERTZ)"  , "de" },
       { "DK (Denmark QWERTY)" , "dk" },
       { "ES (Spanish QWERTY)" , "es" },
       { "FR (French AZERTY)"  , "fr" },
       { "IT (Italian QWERTY)" , "it" },
       { "US (Standard QWERTY)", "us" },
     });
}

SelectorEntry<String>::List MenuProvider::GetTimeZones()
{
  // Get timezones
  static Path timezoneRoot("/usr/share/zoneinfo/posix");
  Path::PathList timezones = timezoneRoot.RecurseDirectoryContent(false);
  // Remove base path
  bool dummy;
  for(Path& tz : timezones) tz = tz.MakeRelative(timezoneRoot, dummy);
  // Sort
  std::sort(timezones.begin(), timezones.end());

  // Build final structure
  SelectorEntry<String>::List result;
  for(Path& tz : timezones)
    result.push_back({ String(tz.ToString()).Replace('/', " - ", 3), tz.ToString() });

  return result;
}

SelectorEntry<String>::List MenuProvider::GetKodiResolutionsEntries()
{
  SelectorEntry<String>::List result;

  result.push_back({ _("NATIVE"), "default" });
  result.push_back({ _("USE GLOBAL"), "" });
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(false))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<ScraperNameOptions>::List MenuProvider::GetScraperNameOptionsEntries()
{
  SelectorEntry<ScraperNameOptions>::List list;
  list.push_back({ _("Scraper results"), ScraperNameOptions::GetFromScraper });
  list.push_back({ _("Raw filename"), ScraperNameOptions::GetFromFilename });
  list.push_back({ _("Undecorated filename"), ScraperNameOptions::GetFromFilenameUndecorated });
  return list;
}

SelectorEntry<ScrapingMethod>::List MenuProvider::GetScrapingMethods()
{
  SelectorEntry<ScrapingMethod>::List list;
  list.push_back({ _("All Games"), ScrapingMethod::All });
  list.push_back({ _("Only missing image"), ScrapingMethod::AllIfNothingExists });
  return list;
}

SelectorEntry<ScraperType>::List MenuProvider::GetScrapersEntries()
{
  SelectorEntry<ScraperType>::List list;
  for(const auto& kv : ScraperFactory::GetScraperList())
    list.push_back({ kv.second, kv.first });
  return list;
}

SelectorEntry<SystemData*>::List MenuProvider::GetScrapableSystemsEntries()
{
  SelectorEntry<SystemData*>::List list;
  for(SystemData* system : mSystemManager.VisibleSystemList())
  {
    if (!system->IsScreenshots()) // Only screenshot doesn't need to be scraped
      if (system->HasScrapableGame())
        list.push_back({ system->FullName(), system, mScraperData.mSystems.Contains(system) });
  }
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List MenuProvider::GetScraperImagesEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List list;
  list.push_back({ _("In-game screenshot"), ScreenScraperEnums::ScreenScraperImageType::ScreenshotIngame });
  list.push_back({ _("Title screenshot"),   ScreenScraperEnums::ScreenScraperImageType::ScreenshotTitle });
  list.push_back({ _("Clear logo"),         ScreenScraperEnums::ScreenScraperImageType::Wheel });
  list.push_back({ _("Marquee"),            ScreenScraperEnums::ScreenScraperImageType::Marquee });
  list.push_back({ _("box2D"),              ScreenScraperEnums::ScreenScraperImageType::Box2d });
  list.push_back({ _("box3d"),              ScreenScraperEnums::ScreenScraperImageType::Box3d });
  list.push_back({ _("ScreenScraper Mix V1"),ScreenScraperEnums::ScreenScraperImageType::MixV1 });
  list.push_back({ _("ScreenScraper Mix V2"), ScreenScraperEnums::ScreenScraperImageType::MixV2 });
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List MenuProvider::GetScraperThumbnailsEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List list;
  list.push_back({ _("No thumbnail"),       ScreenScraperEnums::ScreenScraperImageType::None });
  list.push_back({ _("In-game screenshot"), ScreenScraperEnums::ScreenScraperImageType::ScreenshotIngame });
  list.push_back({ _("Title screenshot"),   ScreenScraperEnums::ScreenScraperImageType::ScreenshotTitle });
  list.push_back({ _("Clear logo"),         ScreenScraperEnums::ScreenScraperImageType::Wheel });
  list.push_back({ _("Marquee"),            ScreenScraperEnums::ScreenScraperImageType::Marquee });
  list.push_back({ _("box2D"),              ScreenScraperEnums::ScreenScraperImageType::Box2d });
  list.push_back({ _("box3d"),              ScreenScraperEnums::ScreenScraperImageType::Box3d });
  list.push_back({ _("ScreenScraper Mix V1"), ScreenScraperEnums::ScreenScraperImageType::MixV1 });
  list.push_back({ _("ScreenScraper Mix V2"), ScreenScraperEnums::ScreenScraperImageType::MixV2 });
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List MenuProvider::GetScraperVideosEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List list;
  list.push_back({ _("No video"), ScreenScraperEnums::ScreenScraperVideoType::None });
  list.push_back({ String(_("Optimized video")).Append(" (").Append(_("RECOMMENDED")).Append(')'), ScreenScraperEnums::ScreenScraperVideoType::Optimized });
  list.push_back({ _("Original video"), ScreenScraperEnums::ScreenScraperVideoType::Raw});
  return list;
}

SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List MenuProvider::GetScraperRegionOptionsEntries()
{
  SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List list;
  list.push_back({_("DETECTED REGION"), ScreenScraperEnums::ScreenScraperRegionPriority::DetectedRegion });
  list.push_back({_("FAVORITE REGION"), ScreenScraperEnums::ScreenScraperRegionPriority::FavoriteRegion });
  return list;
}

SelectorEntry<Regions::GameRegions>::List MenuProvider::GetScraperRegionsEntries()
{
  SelectorEntry<Regions::GameRegions>::List list;
  for(Regions::GameRegions region : Regions::AvailableRegions())
  {
    if(Regions::GameRegions::Unknown == region) continue;
    list.push_back({ Regions::RegionFullName(region), region });
  }
  return list;
}

SelectorEntry<Languages>::List MenuProvider::GetScraperLanguagesEntries()
{
  SelectorEntry<Languages>::List list;
  for(Languages language : LanguagesTools::AvailableLanguages())
  {
    if (language == Languages::Unknown) continue;
    list.push_back({ LanguagesTools::LanguagesFullName(language), language });
  }
  return list;
}

bool MenuProvider::HasScraperCredentials(ScraperType type)
{
  switch(type)
  {
    case ScraperType::ScreenScraper: return true;
    case ScraperType::Recalbox:
    case ScraperType::TheGameDB:
    default: break;
  }
  return false;
}

String MenuProvider::GetScraperLogin(ScraperType type)
{
  switch(type)
  {
    case ScraperType::ScreenScraper: return mConf.GetScreenScraperLogin();
    case ScraperType::Recalbox:
    case ScraperType::TheGameDB:
    default: break;
  }
  return String();
}

String MenuProvider::GetScraperPassword(ScraperType type)
{
  switch(type)
  {
    case ScraperType::ScreenScraper: return mConf.GetScreenScraperPassword();
    case ScraperType::Recalbox:
    case ScraperType::TheGameDB:
    default: break;
  }
  return String();
}

void MenuProvider::SetScraperLogin(ScraperType type, const String& login)
{
  switch(type)
  {
    case ScraperType::ScreenScraper: mConf.SetScreenScraperLogin(login).Save(); break;
    case ScraperType::Recalbox:
    case ScraperType::TheGameDB:
    default: break;
  }
}

void MenuProvider::SetScraperPassword(ScraperType type, const String& password)
{
  switch(type)
  {
    case ScraperType::ScreenScraper: mConf.SetScreenScraperPassword(password).Save(); break;
    case ScraperType::Recalbox:
    case ScraperType::TheGameDB:
    default: break;
  }
}

SelectorEntry<RecalboxConf::SoftPatching>::List& MenuProvider::GetSoftpatchingEntries()
{
  static SelectorEntry<RecalboxConf::SoftPatching>::List list
    {
      { _("AUTO"), RecalboxConf::SoftPatching::Auto },
      { _("LAUNCH LAST"), RecalboxConf::SoftPatching::LaunchLast },
      { _("SELECT"), RecalboxConf::SoftPatching::Select },
      { _("DISABLE"), RecalboxConf::SoftPatching::Disable },
    };
  return list;
}

SelectorEntry<String>::List& MenuProvider::GetSuperGameBoyEntries()
{
  static SelectorEntry<String>::List list
  {
    { _("GAME BOY"), "gb" },
    { _("SUPER GAME BOY"), "sgb" },
    { _("ASK AT LAUNCH"), "ask" },
  };
  return list;
}

SelectorEntry<String>::List MenuProvider::GetRatioEntries()
{
  SelectorEntry<String>::List list;
  for (const auto& ratio : LibretroRatio::GetRatio())
    list.push_back({ ratio.first, ratio.second });

  return list;
}

SelectorEntry<String>::List& MenuProvider::GetShaderPresetsEntries()
{
  static SelectorEntry<String>::List list
  {
    { _("NONE"), "none" },
    { _("CRT CURVED"), "crtcurved" },
    { _("SCANLINES"), "scanlines" },
    { _("RETRO"), "retro" },
  };
  return list;
}

SelectorEntry<String>::List MenuProvider::GetShadersEntries()
{
  SelectorEntry<String>::List list;
  MenuTools::ShaderList shaderList = MenuTools::ListShaders();
  list.push_back({ _("NONE"), "" });
  for (const MenuTools::Shader& shader : shaderList)
    list.push_back({ shader.Displayable, shader.ShaderPath.ToString() });
  return list;
}

SelectorEntry<RecalboxConf::Relay>::List& MenuProvider::GetMitmEntries()
{
  static SelectorEntry<RecalboxConf::Relay>::List list
  {
    { _("NONE"), RecalboxConf::Relay::None },
    { _("NEW YORK"), RecalboxConf::Relay::NewYork },
    { _("MADRID"), RecalboxConf::Relay::Madrid },
    { _("MONTREAL"), RecalboxConf::Relay::Montreal },
    { _("SAOPAULO"), RecalboxConf::Relay::Saopaulo },
  };
  return list;
}

SelectorEntry<RecalboxConf::UpdateType>::List MenuProvider::GetUpdateTypeEntries()
{
  SelectorEntry<RecalboxConf::UpdateType>::List list;

  list.push_back({ "stable", RecalboxConf::UpdateType::Stable });
  list.push_back({ "patron", RecalboxConf::UpdateType::Patron });
  #if defined(BETA) || defined(DEBUG)
  list.push_back({ "alpha" , RecalboxConf::UpdateType::Alpha });
  list.push_back({ "jamma" , RecalboxConf::UpdateType::Jamma });
  #endif

  return list;
}

SelectorEntry<RecalboxConf::Screensaver>::List MenuProvider::GetTypeEntries()
{
  SelectorEntry<RecalboxConf::Screensaver>::List list;

  RecalboxConf::Screensaver type = mConf.GetScreenSaverType();
  if (Board::Instance().HasSuspendResume())
    list.push_back({ _("suspend"), RecalboxConf::Screensaver::Suspend, type == RecalboxConf::Screensaver::Suspend });
  list.push_back({ _("dim"), RecalboxConf::Screensaver::Dim, type == RecalboxConf::Screensaver::Dim });
  list.push_back({ _("black"), RecalboxConf::Screensaver::Black, type == RecalboxConf::Screensaver::Black });
  list.push_back({ _("demo"), RecalboxConf::Screensaver::Demo, type == RecalboxConf::Screensaver::Demo });
  list.push_back({ _("gameclip"), RecalboxConf::Screensaver::Gameclip, type == RecalboxConf::Screensaver::Gameclip });

  return list;
}

SelectorEntry<String>::List MenuProvider::GetSystemListAsString()
{
  SelectorEntry<String>::List list;

  for (const SystemData* system : mSystemManager.AllSystems())
    if (system->HasGame() && !system->Descriptor().IsPort())
      list.push_back({ system->FullName(), system->Name(), mConf.IsInScreenSaverSystemList(system->Name()) });

  return list;
}

SelectorEntry<String>::List MenuProvider::GetThemeTransitionEntries()
{
  SelectorEntry<String>::List list;

  String originalTransition = mConf.GetThemeTransition();
  list.push_back({ _("FADE"), "fade", originalTransition == "fade" });
  list.push_back({ _("SLIDE"), "slide", originalTransition == "slide" });
  list.push_back({ _("INSTANT"), "instant", originalTransition == "instant" });

  return list;
}

SelectorEntry<String>::List MenuProvider::GetThemeRegionEntries()
{
  SelectorEntry<String>::List list;

  String region = mConf.GetThemeRegion();
  list.push_back({ _("Europe"), "eu", region == "eu" });
  list.push_back({ _("USA"), "us", region == "us" });
  list.push_back({ _("Japan"), "jp", region == "jp" });

  return list;
}

ThemeSpec MenuProvider::GetCurrentTheme()
{
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();
  if (themelist.contains(mConf.GetThemeFolder())) return { mConf.GetThemeFolder(), themelist[mConf.GetThemeFolder()] };
  return { String::Empty, Path::Empty };
}

ThemeSpec MenuProvider::GetDefaultTheme()
{
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();
  if (themelist.contains(ThemeManager::sDefaultThemeFolder)) return { ThemeManager::sDefaultThemeFolder, themelist[ThemeManager::sDefaultThemeFolder] };
  return { themelist.begin()->first, themelist.begin()->second };
}

String MenuProvider::GetDisplayList(ThemeData::Compatibility display)
{
  String result;
  if ((display & ThemeData::Compatibility::Hdmi ) != 0) result = "HDMI";
  if ((display & ThemeData::Compatibility::Crt  ) != 0) { if (!result.empty()) result.Append(','); result.Append("CRT"); };
  if ((display & ThemeData::Compatibility::Jamma) != 0) { if (!result.empty()) result.Append(','); result.Append("JAMMA"); };
  return result;
}

String MenuProvider::GetResolutionList(ThemeData::Resolutions resolutions)
{
  String result;
  if ((resolutions & ThemeData::Resolutions::QVGA) != 0) result = "QVGA (240p)";
  if ((resolutions & ThemeData::Resolutions::VGA ) != 0) { if (!result.empty()) result.Append(','); result.Append("VGA (480p)"); };
  if ((resolutions & ThemeData::Resolutions::HD  ) != 0) { if (!result.empty()) result.Append(','); result.Append("HD (720p)"); };
  if ((resolutions & ThemeData::Resolutions::FHD ) != 0) { if (!result.empty()) result.Append(','); result.Append("FHD (1080p)"); };
  return result;
}

SelectorEntry<SystemSorting>::List& MenuProvider::GetSystemSortingEntries()
{
  static SelectorEntry<SystemSorting>::List list
  {
     { _("DEFAULT")                                    , SystemSorting::Default                                    },
     { _("Name")                                       , SystemSorting::Name                                       },
     { _("RELEASE DATE")                               , SystemSorting::ReleaseDate                                },
     { _("TYPE, THEN NAME")                            , SystemSorting::SystemTypeThenName                         },
     { _("TYPE, THEN RELEASE DATE")                    , SystemSorting::SystemTypeThenReleaseDate                  },
     { _("MANUFACTURER, THEN NAME")                    , SystemSorting::ManufacturerThenName                       },
     { _("MANUFACTURER, THEN RELEASE DATE")            , SystemSorting::ManufacturerThenReleaseData                },
     { _("TYPE, THEN MANUFACTURER, THEN NAME")         , SystemSorting::SystemTypeThenManufacturerThenName         },
     { _("TYPE, THEN MANUFACTURER, THEN RELEASE DATE") , SystemSorting::SystemTypeThenManufacturerThenReleasdeDate },
   };
  return list;
}

SelectorEntry<SystemData*>::List MenuProvider::GetHidableSystems()
{
  SelectorEntry<SystemData*>::List result;
  for(SystemData* system : mSystemManager.AllSystems())
    if (!system->IsPorts() && !system->IsVirtual())
      result.push_back({ system->FullName(), system, mConf.GetSystemIgnore(*system) });
  return result;
}

SelectorEntry<ThemeSpec>::List MenuProvider::GetThemeEntries()
{
  // Get theme list
  ThemeManager::ThemeList themelist = ThemeManager::AvailableThemes();

  String currentVersionString = PROGRAM_VERSION_STRING;
  int cut = (int)currentVersionString.find_first_not_of("0123456789.");
  if (cut >= 0) currentVersionString.Delete(cut, INT32_MAX);

  // Sort names
  ThemeSpecList sortedNames;
  for (const auto& kv : themelist) sortedNames.push_back({ kv.first, kv.second });
  std::sort(sortedNames.begin(), sortedNames.end(), [](const ThemeSpec& a, const ThemeSpec& b) { return a.Name.ToLowerCase() < b.Name.ToLowerCase(); });

  SelectorEntry<ThemeSpec>::List list;
  for (const ThemeSpec& theme : sortedNames)
  {
    bool compatible = false;
    String displayableName = CheckCompatibility(theme.FolderPath, compatible, false);

    // (v) - compatibility match else /!\ sign - compatibility mismatch
    displayableName.Insert(0, compatible ? "\uF1C0 " : "\uF1CA ");

    list.push_back({ displayableName, theme });
  }

  return list;
}

SelectorEntry<String>::List MenuProvider::GetManufacturersVirtualEntries()
{
  SelectorEntry<String>::List result;
  const RecalboxConf& conf = RecalboxConf::Instance();

  for(const std::pair<String, String>& rawIdentifier : ArcadeVirtualSystems::GetVirtualArcadeSystemListExtended())
  {
    String identifier(SystemManager::sArcadeManufacturerPrefix);
    identifier.Append(rawIdentifier.first).Replace('\\', '-');
    result.push_back({ rawIdentifier.second, identifier, conf.IsInCollectionArcadeManufacturers(identifier) });
  }
  return result;
}

SelectorEntry<String>::List MenuProvider::GetBootSystemEntries()
{
  SelectorEntry<String>::List list;

  // For each activated system
  for (SystemData* system : mSystemManager.VisibleSystemList())
    list.push_back({ system->FullName(), system->Name() });

  return list;
}

SelectorEntry<GameGenres>::List MenuProvider::GetGameGenreEntries()
{
  SelectorEntry<GameGenres>::List list;

  for(const GameGenres genre : Genres::GetOrderedList())
  {
    String longName = Genres::GetFullName(genre);
    String prefix = Genres::IsSubGenre(genre) ? "   \u21B3 " : "";
    //Path icon = Path(Genres::GetResourcePath(genre));
    list.push_back({ prefix.Append(_S(longName)), genre, mConf.IsInCollectionGenre(SystemManager::BuildGenreSystemName(genre)) });
  }
  return list;
}

SelectorEntry<String>::List MenuProvider::GetGlobalResolutionEntries()
{
  SelectorEntry<String>::List result;
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(true))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<String>::List MenuProvider::GetResolutionEntries()
{
  SelectorEntry<String>::List result;
  result.push_back({ _("USE GLOBAL").Append(" (").Append(MenuTools::GetResolutionText(mConf.GetGlobalVideoMode())).Append(')'), "" });
  result.push_back({ _("NATIVE").Append(" (").Append(MenuTools::GetResolutionText("default")).Append(')'), "default" });
  for(const ResolutionAdapter::Resolution& resolution : mResolutionAdapter.Resolutions(true))
  {
    String reso = resolution.ToRawString();
    result.push_back({ resolution.ToString(), reso });
  }
  return result;
}

SelectorEntry<Overclocking::Overclock>::List MenuProvider::GetOverclockEntries()
{
  SelectorEntry<Overclocking::Overclock>::List list;

  // Add entries
  const Overclocking::List& oc = Overclocking().GetOverclockLost();
  for(const Overclocking::Overclock& overclock : oc)
  {
    String label = String(overclock.Description()).Append(" (", 2).Append(overclock.Frequency()).Append(" Mhz)", 5).Append(overclock.Hazardous() ? " \u26a0" : "");
    list.push_back({ label, overclock, false });
  }
  std::sort(list.begin(), list.end(), [](const SelectorEntry<Overclocking::Overclock>& a, const SelectorEntry<Overclocking::Overclock>& b) { return a.mValue.Frequency() < b.mValue.Frequency(); });
  // Add none
  if (!list.empty())
    list.insert(list.begin(), { _("NONE"), Overclocking().NoOverclock(), false });

  return list;
}

SelectorEntry<String>::List MenuProvider::GetEmulatorEntries(const SystemData* system, String& emulatorAndCore, String& defaultEmulatorAndCore)
{
  SelectorEntry<String>::List list;

  String currentEmulator(mConf.GetSystemEmulator(*system));
  String currentCore    (mConf.GetSystemCore(*system));
  String dummy;
  MenuTools::EmulatorAndCoreList eList =
    MenuTools::ListEmulatorAndCore(*system, dummy, dummy, currentEmulator, currentCore);
  if (!eList.empty())
    for (const MenuTools::EmulatorAndCore& emulator : eList)
    {
      list.push_back({ emulator.Displayable, emulator.Identifier });
      if (emulator.Selected) emulatorAndCore = emulator.Identifier;
      if (defaultEmulatorAndCore.empty()) defaultEmulatorAndCore = emulator.Identifier;
    }

  return list;
}

String MenuProvider::SystemNameWithEmulator(const SystemData* system)
{
  String emulator;
  String core;
  String name(system->FullName());
  if (!Renderer::Instance().Is480pOrLower())
  {
    name.UpperCase();
    EmulatorManager::GetSystemEmulator(*system, emulator, core);
    if (!emulator.empty())
    {
      name.Append(" - ").Append(emulator);
      if (emulator != core) name.Append(' ').Append(core);
    }
  }
  return name;
}

SelectorEntry<CrtAdapterType>::List MenuProvider::GetDacEntries()
{
  SelectorEntry<CrtAdapterType>::List list;

  if(Board::Instance().CrtBoard().GetCrtAdapter() == CrtAdapterType::RGBDual)
  {
    list.push_back( { "Recalbox RGB Dual", CrtAdapterType::RGBDual, true } );
    return list;
  }

  CrtAdapterType selectedDac = CrtConf::Instance().GetSystemCRT();

  static struct
  {
    const char* Name;
    CrtAdapterType Type;
  }
    Adapters[] =
    {
      { "Recalbox RGB Dual", CrtAdapterType::RGBDual },
      { "Recalbox RGB Jamma", CrtAdapterType::RGBJamma },
      { "VGA666", CrtAdapterType::Vga666 },
      { "RGBPi", CrtAdapterType::RGBPi },
      { "Pi2SCART", CrtAdapterType::Pi2Scart },
    };

  // Always push none
  list.push_back( { _("NONE"), CrtAdapterType::None, selectedDac == CrtAdapterType::None } );
  // Push all adapters or only one if it is automatically detected
  const ICrtInterface& crt = Board::Instance().CrtBoard();
  for(const auto& item : Adapters)
  {
    if (!crt.HasBeenAutomaticallyDetected() || crt.GetCrtAdapter() == item.Type)
      list.push_back( { item.Name, item.Type, selectedDac == item.Type } );
  }

  return list;
}

SelectorEntry<String>::List MenuProvider::GetEsResolutionEntries()
{
  SelectorEntry<String>::List list;
  ICrtInterface::HorizontalFrequency currentScreen = Board::Instance().CrtBoard().GetHorizontalFrequency();

  std::string baseRez = CrtConf::Instance().GetSystemCRTResolution();
  if(baseRez == "0")
  {
    switch(currentScreen){
      case ICrtInterface::HorizontalFrequency::Auto:
      case(ICrtInterface::HorizontalFrequency::KHz15): baseRez = "240"; break;
      case(ICrtInterface::HorizontalFrequency::KHzMulti1525): baseRez = "384"; break;
      case(ICrtInterface::HorizontalFrequency::KHzTriFreq):
      case(ICrtInterface::HorizontalFrequency::KHzMulti1531):
      case(ICrtInterface::HorizontalFrequency::KHz31): baseRez = "480"; break;
    }
  }

  // Each monitor > KHz31 has 480p support
  if(currentScreen >= ICrtInterface::HorizontalFrequency::KHz31)
  {
    list.push_back({ "480p", "480", baseRez == "480" });
    if(Board::Instance().CrtBoard().Has120HzSupport())
      list.push_back({ "240p@120Hz", "240", baseRez == "240" });
  }

  // Tri and multi with 25
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHzTriFreq || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1525)
    list.push_back({ "384p", "384", baseRez == "384" });

  // All with 15khz
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHzTriFreq
     || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1525
     || currentScreen == ICrtInterface::HorizontalFrequency::KHz15
     || currentScreen == ICrtInterface::HorizontalFrequency::KHzMulti1531)
    list.push_back({ "240p", "240", baseRez == "240" });

  // All with 15khz
  if(currentScreen == ICrtInterface::HorizontalFrequency::KHz15 && Board::Instance().CrtBoard().HasInterlacedSupport())
    list.push_back({ "480i", "480", baseRez == "480" });

  return list;
}

const SelectorEntry<ICrtInterface::HorizontalFrequency>::List& MenuProvider::GetHorizontalOutputFrequency()
{
  static SelectorEntry<ICrtInterface::HorizontalFrequency>::List result
  {
    { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHz15), ICrtInterface::HorizontalFrequency::KHz15 },
    { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHz31), ICrtInterface::HorizontalFrequency::KHz31 },
    { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzMulti1525), ICrtInterface::HorizontalFrequency::KHzMulti1525 },
    { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzMulti1531), ICrtInterface::HorizontalFrequency::KHzMulti1531 },
    { ICrtInterface::HorizontalFrequencyToHumanReadable(ICrtInterface::HorizontalFrequency::KHzTriFreq), ICrtInterface::HorizontalFrequency::KHzTriFreq },
  };
  return result;
}

const SelectorEntry<String>::List& MenuProvider::GetSuperRezEntries()
{
  static SelectorEntry<String>::List list
  {
    { "X6 (DEFAULT)", "1920" },
    { "X8", "2560" },
    { "DYNAMIC", "1" },
    { "NATIVE", "0" },
  };
  return list;
}

const SelectorEntry<CrtScanlines>::List& MenuProvider::GetScanlinesEntries()
{
  static SelectorEntry<CrtScanlines>::List list
  {
    { "NONE", CrtScanlines::None },
    { "LIGHT", CrtScanlines::Light },
    { "MEDIUM", CrtScanlines::Medium },
    { "HEAVY", CrtScanlines::Heavy },
  };
  return list;
}

/*
 * ================================================================================================================
 * TOOLS
 * ================================================================================================================
 */

bool MenuProvider::TrySortNumerically(SelectorEntry<String>::List& list)
{
  HashMap<String, int> nameToNumeric;
  for(const SelectorEntry<String>& item : list)
  {
    if (item.mText.empty()) return false;
    size_t pos = item.mText.find_first_not_of("0123456789");
    if (pos == 0) return false;
    nameToNumeric[item.mText] = (pos == std::string::npos) ? item.mText.AsInt() : item.mText.AsInt(item.mText[pos]);
  }

  std::sort(list.begin(), list.end(), [&nameToNumeric] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return nameToNumeric[a.mText] < nameToNumeric[b.mText]; });
  return true;
}

void MenuProvider::ChangeThemeOptionSelector(ItemSelector<String>& item, const String& selected, const String::List& items)
{
  bool found = false;
  String realSelected;
  for(const String& s : items) if (s == selected) { found = true; realSelected = s; break; }
  if (!found && !items.empty()) realSelected = items.front();

  // Build list
  SelectorEntry<String>::List list;
  for (const String& s : items) list.push_back({ s, s, s == realSelected });
  // Try numeric sorting, else  use an alphanumeric sort
  if (!TrySortNumerically(list))
    std::sort(list.begin(), list.end(), [] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return a.mText < b.mText; });

  if (!items.empty())
    item.ChangeSelectionItems(list, realSelected, list.front().mValue).SetSelectable(true);
  else
    item.ChangeSelectionItems({{ _("OPTION NOT AVAILABLE"), "" }}, "", "").SetSelectable(false);
}

String MenuProvider::CheckCompatibility(const Path& themePath, [[out]] bool& compatible, bool switchTheme)
{
  // Current state
  bool tate = mResolver.IsTate();
  ThemeData::Compatibility currentCompatibility = (mResolver.HasJamma() ? ThemeData::Compatibility::Jamma : ThemeData::Compatibility::None) |
                                                  (mResolver.HasCrt() ? ThemeData::Compatibility::Crt : ThemeData::Compatibility::None) |
                                                  (mResolver.HasHDMI() ? ThemeData::Compatibility::Hdmi : ThemeData::Compatibility::None);
  ThemeData::Resolutions currentResolution = mResolver.IsQVGA() ? ThemeData::Resolutions::QVGA :
                                             mResolver.IsVGA() ? ThemeData::Resolutions::VGA :
                                             mResolver.IsHD() ? ThemeData::Resolutions::HD :
                                             ThemeData::Resolutions::FHD;

  // Fetched data
  int recalboxVersion = 0; // Unknown version
  int themeVersion = 0; // Unknown version
  ThemeData::Compatibility compatibility = ThemeData::Compatibility::Hdmi;
  ThemeData::Resolutions resolutions = ThemeData::Resolutions::HD | ThemeData::Resolutions::FHD;
  String displayableName;

  if (ThemeData::FetchCompatibility(themePath / ThemeManager::sRootThemeFile, compatibility, resolutions, displayableName, themeVersion, recalboxVersion))
  {
    compatible = ((currentCompatibility & compatibility) != 0) &&
                 ((currentResolution & resolutions) != 0) &&
                 (!tate || hasFlag(compatibility, ThemeData::Compatibility::Tate)) &&
                 (recalboxVersion <= sRecalboxMinimumCompatibilityVersion);
    if (switchTheme)
    {
      if (!compatible)
      {
        String modeIssue = (currentCompatibility & compatibility) == 0 ?
                           String("- ").Append(_("You display {0} is not in the list of this theme's supported displays:"))
                                       .Replace("{0}",GetDisplayList(currentCompatibility))
                                       .Append(' ').Append(GetDisplayList(compatibility)).Append(String::CRLF) : String::Empty;
        String resolutionIssue = (currentResolution & resolutions) == 0 ?
                                 String("- ").Append(_("You current resolution {0} is not in the list of this theme's supported resolutions:"))
                                             .Replace("{0}",GetResolutionList(currentResolution))
                                             .Append(' ').Append(GetResolutionList(resolutions)).Append(String::CRLF) : String::Empty;
        String tateIssue = tate && !hasFlag(compatibility, ThemeData::Compatibility::Tate) ?
                           String("- ").Append(_("You're in TATE mode and this theme does not seem to support TATE.")).Append(String::CRLF) : String::Empty;
        String message = _("This theme may have one or more compatibility issues with your current display:\n").Append(modeIssue).Append(resolutionIssue).Append(tateIssue).Append(String::CRLF).Append("Are you sure to activate this theme?");
        mWindow.pushGui(new GuiMsgBox(mWindow, message, _("YES"), [this]{ ThemeManager::Instance().DoThemeChange(&mWindow); }, _("NO"), {}));
      }
      else
      {
        ThemeManager::Instance().DoThemeChange(&mWindow);
      }
    }
    if (themeVersion != 0) displayableName.Append(" (").Append(themeVersion >> 8).Append('.').Append(themeVersion & 0xFF).Append(')');
    return displayableName;
  }

  compatible = false;
  return String::Empty;
}

void MenuProvider::CheckHighLevelFilter(bool& value, SetBoolMethod setter, const char* message, bool inverted)
{
  (mConf.*setter)(inverted ? !value : value);
  if (mSystemManager.UpdatedTopLevelFilter()) mConf.Save();
  else
  {
    mWindow.displayMessage(message == nullptr ? _("There is no game to show after this filter is changed! No change recorded.") : _(message));
    value = !value;
    (mConf.*setter)(inverted ? !value : value);
  }
}

void MenuProvider::DoJammaReset(MenuBuilder& menu)
{
  // recalbox.conf
  RecalboxConf::Instance().ResetWithFallback();
  // Set jamma config to default
  RecalboxConf::Instance().SetGlobalRewind(false);
  RecalboxConf::Instance().SetGlobalSmooth(false);
  RecalboxConf::Instance().SetQuickSystemSelect(false);
  RecalboxConf::Instance().SetAutoPairOnBoot(false);
  RecalboxConf::Instance().SetThemeFolder("recalbox-240p");
  RecalboxConf::Instance().SetThemeIconSet("recalbox-240p", "4-jamma");
  RecalboxConf::Instance().SetThemeGamelistView("recalbox-240p", "3-240p-large-names");
  RecalboxConf::Instance().SetGlobalHidePreinstalled(true);
  RecalboxConf::Instance().SetAutoPairOnBoot(false);
  RecalboxConf::Instance().SetShowGameClipClippingItem(false);
  RecalboxConf::Instance().SetShowGameClipHelpItems(false);
  RecalboxConf::Instance().SetGlobalDemoInfoScreen(0);

  std::vector<String> manufacturers;
  for(const String& rawIdentifier : ArcadeVirtualSystems::GetVirtualArcadeSystemList())
  {
    String identifier(SystemManager::sArcadeManufacturerPrefix);
    identifier.Append(rawIdentifier).Replace('\\', '-');
    manufacturers.push_back(identifier);
  }
  RecalboxConf::Instance().SetCollectionArcadeManufacturers(manufacturers);
  RecalboxConf::Instance().SetGlobalHidePreinstalled(true);
  RecalboxConf::Instance().SetKodiEnabled(false);
  RecalboxConf::Instance().SetSplashEnabled(false);
  RecalboxConf::Instance().Save();

  // recalbox-crt-options.cfg
  CrtConf::Instance().ResetWithFallback();
  CrtConf::Instance().SetSystemCRT(CrtAdapterType::RGBJamma);
  CrtConf::Instance().Save();
  // REBOOT
  menu.RequestReboot();
}

void MenuProvider::RunScreenCalibration()
{
  if (Renderer::Instance().IsRotatedSide())
  {
    mWindow.pushGui(new GuiMsgBox(mWindow, _("Screen calibration only available in YOKO mode."), _("Ok"), [] {}));
    return;
  }
  if (Board::Instance().CrtBoard().GetHorizontalFrequency() == ICrtInterface::HorizontalFrequency::KHz31)
  {
    if (Board::Instance().CrtBoard().GetCrtAdapter() == CrtAdapterType::RGBJamma)
    {
      ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz31_no_120);
      mWindow.CloseAll();
    }
    else
    {
      ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz31);
      mWindow.CloseAll();
    }
  }
  else if (Board::Instance().CrtBoard().MustForce50Hz())
  {
    ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_50Hz);
    mWindow.CloseAll();
  }
    /*else if (Board::Instance().CrtBoard().MultiSyncEnabled())
    {
      ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_60Hz_plus_kHz31);
      mWindow.CloseAll();
    }*/
  else if (Board::Instance().CrtBoard().GetCrtAdapter() == CrtAdapterType::RGBJamma)
  {
    ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_60Hz);
    mWindow.CloseAll();
  }
  else
  {
    mWindow.pushGui(new GuiMsgBox(mWindow, _("You will now calibrate different resolutions for your TV. Select the refresh rate according to what your TV supports.\nDuring the calibration, press B to validate, and A to cancel."),
                                  _("60Hz & 50Hz"), [this] { ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_60plus50Hz); mWindow.CloseAll(); },
                                  _("60Hz Only"), [this] { ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_60Hz); mWindow.CloseAll(); },
                                  _("50Hz Only"), [this] { ViewController::Instance().goToCrtView(CrtCalibrationView::CalibrationType::kHz15_50Hz); mWindow.CloseAll();},
                                  TextAlignment::Center));
  }
}

/*
 * ================================================================================================================
 * MENU EVENT PROCESSING
 * ================================================================================================================
 */

#define LICENSE_ORIGINAL_TEXT "Redistribution and use of the RECALBOX code or any derivative works are permitted provided that the following conditions are met:\n" \
                              "\n" \
                              "- Redistributions may not be sold without authorization, nor may they be used in a commercial product or activity.\n" \
                              "- Redistributions that are modified from the original source must include the complete source code, including the source code for all components used by a binary built from the modified sources.\n" \
                              "- Redistributions must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.\n" \
                              "\n" \
                              "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

static const char* LICENCE_MSG = "\317'\371\066\353;\231\354W\213\261\061\222\066:2d\302`\205\312\345\063\327u\365z\255\004\333\371\061x\217?\214ߑ\240\177\331\002J\326\346\\\251*\352B\367\025D\230\330\062\210\060oI\223\262[\263\327\a\214\204\236\263\237\221=\202\345\rN\316+\337\030\225\034z\315$r\206\020~UC\030F\267}y\276\370\020\263_\236G\215\tq\246\366x\210\377\205ɏ\351P\274\355?3\307\071\303H\314=\200\264\064\r\343AT\004G5`\333;\220\240\374h\250s\260d\356\ae\315]*\270\b\367\373\267\251V\320\003\\\337\345^\346<\342\061\224ZK\206\316%\235?f2\236\357\\\212\300Y(\303g\270#\327\067\211\200\035X\246\021\343\347\333Ud\330>@\313\027M^!YS\243\v{\272\324Z\267X\227\024\233\365N\265\357\002\200\271\233\304\317\307!\250\345#u\333-\201{\207A\257\362#y\244\036}\003N\020h\321=l\270\262Q\242D\223\000\266\001h\235\353\"\204\354\206˾\366\016\256\n.e\273\064\363\070\266\"\233\352T\225\255\067\227vk>}\326]\213\227\023d\305e\363e\255<\225\334\026\\\354\005\244\353\347\035u\331\024A\221\363I\247:\355L\247\fz\310\314-\203'(\035\235\376I\266\326\016\224̚\237\214\307.\305\346\061P\312>\335Z\320\005b\305p\177\217\001\070\022Y\002\005\242\064f\244\364@\266@\204Z\300\te\243\353v\213\367\311\312\304\371^\277\340\070h\204+\321\017\363\"\322\352Nj\241\177p7Dz|\302#\236\246\356\177\333:\257z\271\032a\327\bl\337W\365ŵ\343J\316\031D\224\362@\344;\344,\255XE\204\313?\320\071h6\217\366W\226\317\026&\337n\255!\331\066\201\350\002\016\242\t\261\372\316\032h\301-\002\207\023U\025]\022\002\352\020|\264\327\027\260^\212F\311\352y\357\373\060\234\363\233\300\204\313.\251\252\062t\311r\331\a\315\177\235\372)b\272\037d\006MC~\337#c\245\361J\245F\227>\274\037+\317\354.\376\347\261ì\375I\250\002^r\360Q\367{\365%\203\256^\210\341;\215<s=&\317]\230\320\016f\325c\242*\261/\230\321\031S\257\v\352\373\254\bp\222\026C\227\245N\354\064\364E\272\017h\222\304/\213n\b1\246\306a\213\201;\264\346\257\352\267\377\f\303\332\017\003\346\t\371.\370,A\332nU\207QB9vL`\360\tP\200\320k\231x\316o\221\066P\201\337\a\307ͩ\355\302\332z\201+\032F\340\022\366+\317\aШ{P\352\024Q\177?\031A\357e\264\360\334\070\370@\212A\217\065S\220\066\022\363\061\236ł\314d\377Kc\272\334a\317\036\335\t\231%\n\317\377\a\255\005^\000\262\331v͊3Q\375$\210\260\342r\240\325(e\353!\373\241\356>!\207\003h\242ZV7l9e\215&2\214\343g\210t\271d\256\306N\355\331\033\320ص\340\250\352\177\226\334\020Z\342\034\342\071\377}\270\335\a,\225/_(m2K\224\b\261\225\256`\346q\242N\207&J\354\316\b\216\241\214˅\336e\227\061\035E\305y\267\035\333\032\246\063n\261\362\027\263|4\004J\200t\245\223?[\367C\233(\354\023\264\351\"\025\234%\215\252\200\065F\351\062`\267\313|\243\n){\224\071;\253\371\036\240qNm\242\305d\203\313)\253\315\331\371\261\213\v\250\320\030m\361y\362\071\375FC\322\025\065\230&F*r>\r\235\036I\224\336x\212i\270\t\374\061\\\201\302\026\240ı欻\023\236-\003N\351\004\354N\273\027\266\315uQ\204\032R\002\061vB\373c\260\363\315E\376G\215T\206\060\267\357;\036\225\067\234ҋ\306L\231A{\267\317}\331\n\316p\235X\004\257\341\023\314\tF\026\331\331f\222\341;G\343\"\220\262\230p\272\310,i\350\060\370\062\375!_\205\032X\305+H:i'c\222\065D\234\200t\227|\261}\305\316I\353\307\036\274ŧ\363\254\363\a\366\312\000E\372n\346-\204\016\244\324m*\225%]%\n\177Y\340l\254\227\303Y\215K\262!\361\"U\227\302\022\211\066\224ن\337\b\213<oG\307h\300\a\304\016\254\063\004\326\365\037\242\025D\016P\276{\251\344+N\364+\214H\371y\274\355Lr\210\062\346܉=[\374\a\a\241\307\006\315\n=q\206%P\266\360\020\301k\266r\272\302a\206\275-\271\274\264\366\274\375\027\236\314\f3\222\004\346&\344'T\272\006Z\233/[!f<F\227uJ\234\265|\202z\276\003\362?^\203\305\005\247\301\255\342\376\333t\214\067\rL\333\r\335%\271\037\276\246y\253\216\034\254\016R\aN\216a\260\350\310G\360Z\221/\217,\334\355;e\357#\216\310\352\316H\343G\177\261\252{\302\t\245i\213\067\002\244\364e\276\rN\023\327\300b\233\362:A\367E\356\333\367\030\255\326Aa\375@\372\071\340$Z\346\027<\254\060;\"p4\b\224\021]\204\327s\237}\272x\272_V\217\262\n\271Ҥ\216\243\337w\225\303\b:";
static const int LICENCE_MSG_SIZE = (int)sizeof(LICENSE_ORIGINAL_TEXT) - 1;
static const char MESSAGE_DECORATOR[] = "\x9D\x31\x7B\x2C\x54\xFA\x85\x0E\xAD\x65\x1B";

static String GetText()
{
  String buffer(LICENCE_MSG, LICENCE_MSG_SIZE);
  int l = strlen(MESSAGE_DECORATOR);
  for (size_t i = 0; i < buffer.size(); ++i)
    buffer[i] = (char)(buffer[i] ^ (MESSAGE_DECORATOR[i % l] + (i*17)));
  return buffer;
}

void MenuProvider::MenuEditableChanged(ItemEditable& item, int id, const String& newText)
{
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionUsername: SetScraperLogin(CoItemAsList<ScraperType>(item, MenuItemType::ScrapeFrom).SelectedValue(), newText); break;
    case MenuItemType::ScrapeOptionPassword: SetScraperPassword(CoItemAsList<ScraperType>(item, MenuItemType::ScrapeFrom).SelectedValue(), newText); break;
    case MenuItemType::RetroachievementsUsername: mConf.SetRetroAchievementLogin(newText).Save(); break;
    case MenuItemType::RetroachievementsPassword: mConf.SetRetroAchievementPassword(newText).Save(); break;
    case MenuItemType::NetplayNickname: mConf.SetNetplayLogin(newText).Save(); break;
    case MenuItemType::NetplayPort: { int p = newText.AsInt(0, RecalboxConf::sNetplayDefaultPort); mConf.SetNetplayPort(p).Save(); item.SetText(String(p), false); break; }
    case MenuItemType::NetworkHostname: mConf.SetHostname(newText).Save(); mWindow.displayMessage(_("Hostname changes will not be effective until next reboot")); break;
    case MenuItemType::NetworkSSID:
    case MenuItemType::NetworkPassword:
    {
      if ((MenuItemType)id == MenuItemType::NetworkSSID) mConf.SetWifiSSID(newText);
      else mConf.SetWifiKey(newText);
      mConf.Save();
      if (!mConf.GetWifiSSID().empty() && !mConf.GetWifiKey().empty())
        MenuModalTaskWIFI::CreateWIFITask(mWindow, mConf.GetWifiEnabled());
      break;
    }
    default:
     mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuEditableChanged !");
  }
}

void MenuProvider::MenuActionTriggered(ItemAction& item, int id)
{
  (void)item;
  switch((MenuItemType)id)
  {
    case MenuItemType::Shutdown: mWindow.pushGui(new GuiMsgBox(mWindow, _("REALLY SHUTDOWN?"), _("YES"), []{ MainRunner::RequestQuit(MainRunner::ExitState::Shutdown); }, _("NO"), nullptr)); break;
    case MenuItemType::FastShutdown: mWindow.pushGui(new GuiMsgBox(mWindow, _("REALLY SHUTDOWN WITHOUT SAVING METADATAS?"), _("YES"), []{ MainRunner::RequestQuit(MainRunner::ExitState::FastShutdown); }, _("NO"), nullptr)); break;
    case MenuItemType::Restart: mWindow.pushGui(new GuiMsgBox(mWindow, _("REALLY RESTART?"), _("YES"), []{ MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); }, _("NO"), nullptr)); break;
    case MenuItemType::RunKodi: if (!GameRunner::Instance().RunKodi()) { LOG(LogWarning) << "[Kodi] Kodi terminated with non-zero result!"; } break;
    case MenuItemType::OpenLobby: mWindow.pushGui(new GuiNetPlay(mWindow, mSystemManager)); /*Close();*/ break;
    case MenuItemType::CheckBios: mWindow.pushGui(new GuiBiosScan(mWindow, mSystemManager)); break;
    case MenuItemType::ShowLicense: mWindow.pushGui(new GuiMsgBoxScroll(mWindow, "RECALBOX", GetText(), _("OK"), nullptr, "", nullptr, "", nullptr, TextAlignment::Left)); break;
    case MenuItemType::ScrapeRun:
    {
      if (mScraperData.mSystems.Empty())
        mWindow.pushGui(new GuiMsgBox(mWindow, _("Please select one or more systems to scrape!"), _("OK"), nullptr));
      else
        GuiScraperRun::CreateOrShow(mWindow, mSystemManager, mScraperData.mSystems, mScraperData.mMethod, &GameRunner::Instance(),
                                    Renderer::Instance().Is480pOrLower());
      break;
    }
    case MenuItemType::HashRoms: MenuModalHashRom::CreateRomHasher(mWindow, mSystemManager); break;
    case MenuItemType::UpdateChangelog:
    {
      String changelog = Upgrade::Instance().NewReleaseNote();
      if (!changelog.empty())
        mWindow.displayScrollMessage(_("AN UPDATE IS AVAILABLE FOR YOUR RECALBOX"), _("NEW VERSION:") + ' ' + Upgrade::Instance().NewVersion() + "\n" + _("UPDATE CHANGELOG:") + "\n" + changelog);
      else
        mWindow.displayMessage(_("AN UPDATE IS AVAILABLE FOR YOUR RECALBOX"));
      break;
    }
    case MenuItemType::UpdateStart: mWindow.pushGui(new GuiUpdateRecalbox(mWindow, Upgrade::Instance().TarUrl(), Upgrade::Instance().ImageUrl(), Upgrade::Instance().HashUrl(), Upgrade::Instance().NewVersion())); break;
    case MenuItemType::UpdateCheckNow: Upgrade::Instance().DoManualCheck(); break;
    case MenuItemType::NetworkWPS: MenuModalTaskWPS::CreateWPS(mWindow, CoItemAsEditor(item, MenuItemType::NetworkSSID), CoItemAsEditor(item, MenuItemType::NetworkPassword)); break;
    case MenuItemType::UpdateGamelists: mWindow.pushGui(new GuiMsgBox(mWindow, _("REALLY UPDATE GAMES LISTS ?"), _("YES"), [] { MainRunner::RequestQuit(MainRunner::ExitState::Relaunch, true); }, _("NO"), nullptr)); break;
    case MenuItemType::DeviceInitialize: MenuModalInitDevice::CreateDeviceInitializer(mWindow, item.Context().Device()->ToDeviceMount(), USBInitializationAction::OnlyRomFolders); break;
    case MenuItemType::FactoryReset: MenuModalFactoryReset::InitiateFactoryReset(mWindow); break;
    case MenuItemType::BootloaderUpdate:
    {
      RPiEepromUpdater updater;
      if(updater.Error()) { mWindow.pushGui(new GuiMsgBox(mWindow, _("Could not get bootloader status. Something went wrong"), _("OK"), nullptr)); return; }
      if(updater.IsUpdateAvailable())
      {
        mWindow.pushGui(new GuiMsgBox(mWindow, _("An update is available :\n").Append(_("Current version: \n")).Append(updater.CurrentVersion()).Append(_("\nLatest version: \n")).Append(updater.LastVersion()),
                                      _("UPDATE"), [this, updater]{
            { LOG(LogInfo) << "[EepromUpdate] Processing UPDATE to " << updater.LastVersion(); }
            if(updater.Update())
            {
              mWindow.pushGui(new GuiMsgBox(mWindow, _("Update success. The bootloader is up to date."), _("OK"), [&]{ MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); }));
              { LOG(LogInfo) << "[EepromUpdate] Update success to " << updater.LastVersion(); }
            }
            else
            {
              mWindow.pushGui(new GuiMsgBox(mWindow, _("Could not update bootloader. Something went wrong"), _("OK"), nullptr));
              { LOG(LogError) << "[EepromUpdate] Unable to update to " << updater.LastVersion(); }
            }
          }, _("CANCEL"), nullptr));
      }
      else mWindow.pushGui(new GuiMsgBox(mWindow, _("Your bootloader is up to date.\nThe bootloader version is:\n").Append(updater.CurrentVersion()), _("OK"), nullptr));
      break;
    }
    case MenuItemType::JammaReset: { mWindow.pushGui(new GuiMsgBox(mWindow, _("Are you sure you want to reset JAMMA configuration?"), _("YES"), [this, &item] { DoJammaReset((MenuBuilder&)item.Parent()); }, _("NO"), nullptr)); break; }
    case MenuItemType::CrtScreenCalibration: { RunScreenCalibration(); break; }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuActionTriggered !");
  }
}

void MenuProvider::MenuSwitchChanged(const ItemSwitch& item, bool& value, int id)
{
  switch((MenuItemType)id)
  {
    case MenuItemType::EnableKodi: mConf.SetKodiEnabled(value).Save(); break;
    case MenuItemType::KodiOnStartup: mConf.SetKodiAtStartup(value).Save(); break;
    case MenuItemType::KodiOnX: mConf.SetKodiXButton(value).Save(); break;
    case MenuItemType::ScrapeOptionManual: mConf.SetScreenScraperWantManual(value).Save(); break;
    case MenuItemType::ScrapeOptionMap: mConf.SetScreenScraperWantMaps(value).Save(); break;
    case MenuItemType::ScrapeOptionP2K: mConf.SetScreenScraperWantP2K(value).Save(); break;
    case MenuItemType::Rewind: mConf.SetGlobalRewind(value).Save(); break;
    case MenuItemType::ShowSaveStates: mConf.SetGlobalShowSaveStateBeforeRun(value).Save(); if (value) CoItemAsSwitch(item, MenuItemType::AutoSave).SetState(false, true); break;
    case MenuItemType::AutoSave: mConf.SetGlobalAutoSave(value).Save(); if (value) CoItemAsSwitch(item, MenuItemType::ShowSaveStates).SetState(false, true); break;
    case MenuItemType::PressTwice: mConf.SetGlobalQuitTwice(value).Save(); break;
    case MenuItemType::RecalboxOverlays: mConf.SetGlobalRecalboxOverlays(value).Save(); break;
    case MenuItemType::Smooth: mConf.SetGlobalSmooth(value).Save(); break;
    case MenuItemType::IntegerScale:
    case MenuItemType::IntegerScaleCrt: mConf.SetGlobalIntegerScale(value).Save(); break;
    case MenuItemType::HDMode:
    {
      if (value) mWindow.displayMessage(_("RECALBOX WILL AUTOMATICALLY SELECT EMULATORS TO RUN ON HD-COMPATIBLE SYSTEMS. SOME GAMES MAY NOT BE 100% COMPATIBLE, SO DON'T FORGET TO DISABLE THIS OPTION IF EMULATION OR PERFORMANCE ISSUES APPEAR."));
      mConf.SetGlobalHDMode(value).Save();
      break;
    }
    case MenuItemType::WideScreen:
    {
      if (value) mWindow.displayMessage(_("RECALBOX WILL AUTOMATICALLY SELECT EMULATORS TO RUN ON WIDESCREEN-COMPATIBLE SYSTEMS. SOME GAMES MAY NOT BE 100% COMPATIBLE, SO DON'T FORGET TO DISABLE THIS OPTION IF EMULATION OR PERFORMANCE ISSUES APPEAR."));
      mConf.SetGlobalWidescreenMode(value).Save();
      break;
    }
    case MenuItemType::VulkanDriver: mConf.SetGlobalVulkanDriver(value).Save(); break;
    case MenuItemType::Retroachievements: mConf.SetRetroAchievementOnOff(value).Save(); break;
    case MenuItemType::RetroachievementsHardcore: mConf.SetRetroAchievementHardcore(value).Save(); break;
    case MenuItemType::Netplay: mConf.SetNetplayEnabled(value).Save(); break;
    case MenuItemType::UpdateCheck: mConf.SetUpdatesEnabled(value).Save(); break;
    case MenuItemType::NetworkEnableWIFI: mConf.SetWifiEnabled(value); MenuModalTaskWIFI::CreateWIFITask(mWindow, value); break;
    case MenuItemType::ShowOnlyLastVersions: CheckHighLevelFilter(value, &RecalboxConf::SetShowOnlyLatestVersion, nullptr, false); break;
    case MenuItemType::ShowOnlyFavorites: CheckHighLevelFilter(value, &RecalboxConf::SetFavoritesOnly, "There is no favorite games in any system!", false); break;
    case MenuItemType::ShowFavoriteFirst: mConf.SetFavoritesFirst(value).Save(); break;
    case MenuItemType::ShowHiddenGames: CheckHighLevelFilter(value, &RecalboxConf::SetShowHidden, nullptr, false); break;
    case MenuItemType::ShowMahjongCasino: CheckHighLevelFilter(value, &RecalboxConf::SetHideBoardGames, nullptr, true); break;
    case MenuItemType::ShowAdultGames: CheckHighLevelFilter(value, &RecalboxConf::SetFilterAdultGames, nullptr, true); break;
    case MenuItemType::ShowPreinstalledGames:  CheckHighLevelFilter(value, &RecalboxConf::SetGlobalHidePreinstalled, nullptr, true); break;
    case MenuItemType::Show3PlayerGames: CheckHighLevelFilter(value, &RecalboxConf::SetShowOnly3PlusPlayers, nullptr, false); break;
    case MenuItemType::ShowOnlyYoko: if (ItemSwitch& co = CoItemAsSwitch(item, MenuItemType::ShowOnlyTate); value && co.State()) co.SetState(false, false); CheckHighLevelFilter(value, &RecalboxConf::SetShowOnlyYokoGames, nullptr, false); break;
    case MenuItemType::ShowOnlyTate: if (ItemSwitch& co = CoItemAsSwitch(item, MenuItemType::ShowOnlyYoko); value && co.State()) co.SetState(false, false); CheckHighLevelFilter(value, &RecalboxConf::SetShowOnlyTateGames, nullptr, false); break;
    case MenuItemType::ShowNonGames: CheckHighLevelFilter(value, &RecalboxConf::SetHideNoGames, nullptr, false); break;
    case MenuItemType::ThemeCarousel: mConf.SetThemeCarousel(value).Save(); break;
    case MenuItemType::QuickSelectSystem: mConf.SetQuickSystemSelect(value).Save(); break;
    case MenuItemType::OnScreenHelp: mConf.SetShowHelp(value).Save(); break;
    case MenuItemType::SwapValidateCancel: mConf.SetSwapValidateAndCancel(value).Save(); mWindow.UpdateHelpSystem(); break;
    case MenuItemType::OSDClock: mConf.SetClock(value).Save(); break;
    case MenuItemType::DisplayByFilename:
    {
      mConf.SetDisplayByFileName(value).Save();
      // TODO move the following code in ViewController in OnDisplayByFilenameChanged() notification
      ViewController::Instance().GetOrCreateGamelistView(ViewController::Instance().CurrentSystem())->refreshList();
      ViewController::Instance().InvalidateAllGamelistsExcept(nullptr);
      break;
    }
    case MenuItemType::VirtualAllGames: mConf.SetCollectionAllGames(value).Save(); break;
    case MenuItemType::VirtualMultiplayer: mConf.SetCollectionMultiplayer(value).Save(); break;
    case MenuItemType::VirtualLastPlayed: mConf.SetCollectionLastPlayed(value).Save(); break;
    case MenuItemType::VirtualLightgun: mConf.SetCollectionLightGun(value).Save(); break;
    case MenuItemType::VirtualPorts: mConf.SetCollectionPorts(value).Save(); break;
    case MenuItemType::ArcadeEnhancedView: mConf.SetArcadeViewEnhanced(value).Save(); break;
    case MenuItemType::ArcadeFoldClones: mConf.SetArcadeViewFoldClones(value); break;
    case MenuItemType::ArcadeHideBios: mConf.SetArcadeViewHideBios(value); break;
    case MenuItemType::ArcadeHideNonWorking: mConf.SetArcadeViewHideNonWorking(value); break;
    case MenuItemType::ArcadeUseOfficielNames: mConf.SetArcadeUseDatabaseNames(value); break;
    case MenuItemType::ArcadeAggregateEnable: mConf.SetCollectionArcade(value).Save(); break;
    case MenuItemType::ArcadeAggregateNeoGeo: mConf.SetCollectionArcadeNeogeo(value).Save(); break;
    case MenuItemType::ArcadeAggregateOriginal: mConf.SetCollectionArcadeHideOriginals(value).Save(); break;
    case MenuItemType::BootOnKodi: mConf.SetKodiAtStartup(value).Save(); break;
    case MenuItemType::BootDoNotScan: mConf.SetStartupGamelistOnly(value).Save(); break;
    case MenuItemType::AllowBootOnGame:
    {
      mConf.SetAutorunEnabled(value).Save();
      if (value)
        mWindow.pushGui(new GuiMsgBox(mWindow, _("If no configured controller is detected at boot, Recalbox will run as usual and display the system list."), _("OK")));
      break;
    }
    case MenuItemType::BootOnGamelist: mConf.SetStartupStartOnGamelist(value).Save(); break;
    case MenuItemType::BootShowVideo: mConf.SetSplashEnabled(value).Save(); break;
    case MenuItemType::HideSystemView: mConf.SetStartupHideSystemView(value).Save(); break;
    case MenuItemType::AdvDebugLogs: MainRunner::SetDebugLogs(value, false); mConf.SetDebugLogs(value).Save(); break;
    case MenuItemType::AdvShowFPS: mConf.SetGlobalShowFPS(value).Save(); break;
    case MenuItemType::WebManagerEnable: mConf.SetSystemManagerEnabled(value); break;
    case MenuItemType::PerSystemSmooth: mConf.SetSystemSmooth(*item.Context().System(), value).Save(); break;
    case MenuItemType::PerSystemRewind: mConf.SetSystemRewind(*item.Context().System(), value).Save(); break;
    case MenuItemType::PerSystemAutoLoadSave: mConf.SetSystemAutoSave(*item.Context().System(), value).Save(); break;
    case MenuItemType::Jamma4PlayerMode:
    {
      CrtConf::Instance().SetSystemCRTJamma4Players(value).Save();
      auto setStatus = [this, &value]{ CheckHighLevelFilter(value, &RecalboxConf::SetShowOnly3PlusPlayers, nullptr, false); };
      if (value && !mConf.GetShowOnly3PlusPlayers())
        mWindow.pushGui(new GuiMsgBox(mWindow, _("Do you want to enable the 3+ player filter for all the games ?"), _("NO"), [] {}, _("YES"), setStatus));
      if (!value && mConf.GetShowOnly3PlusPlayers())
        mWindow.pushGui(new GuiMsgBox(mWindow, _("Do you want to disable the 3+ player filter for all the games ?"), _("NO"), [] {}, _("YES"), setStatus));
      break;
    }
    case MenuItemType::JammaCredit: mCrtConf.SetSystemCRTJammaStartBtn1Credit(value).Save(); break;
    case MenuItemType::JammaHotkey: mCrtConf.SetSystemCRTJammaHKOnStart(value).Save(); break;
    case MenuItemType::JammaVolume: mCrtConf.SetSystemCRTJammaSoundOnStart(value).Save(); break;
    case MenuItemType::JammaExit: mCrtConf.SetSystemCRTJammaExitOnStart(value).Save(); break;
    case MenuItemType::JammaAutoFire: mCrtConf.SetSystemCRTJammaAutoFire(value).Save(); break;
    case MenuItemType::JammaDualJoystick: mCrtConf.SetSystemCRTJammaDualJoysticks(value).Save(); break;
    case MenuItemType::JammaPinE27: mCrtConf.SetSystemCRTJammaButtonsOnJamma(value ? "5" : "6").Save(); break;
    case MenuItemType::CrtHDMIPriority: mCrtConf.SetSystemCRTForceHDMI(value).Save(); break;
    case MenuItemType::CrtSelectRefreshAtLaunch: mCrtConf.SetSystemCRTGameRegionSelect(value).Save(); break;
    case MenuItemType::CrtSelectResolutionAtLaunch: mCrtConf.SetSystemCRTGameResolutionSelect(value).Save(); break;
    case MenuItemType::CrtForceJack: mCrtConf.SetSystemCRTForceJack(value).Save(); break;
    case MenuItemType::CrtDemo240: mCrtConf.SetSystemCRTRunDemoIn240pOn31kHz(value).Save(); break;
    case MenuItemType::CrtReducedLantency: mConf.SetGlobalReduceLatency(value).Save(); break;
    case MenuItemType::CrtRunAhead: mConf.SetGlobalRunAhead(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSwitchChanged !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<StorageDevices::Device>& item, int id, int index, const StorageDevices::Device& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ShareDevice: mStorageDevices.SetShareDevice(value); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<StorageDevices::Device> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<String>& item, int id, int index, const String& value)
{
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::Language: mConf.SetSystemLanguage(value).Save(); break;
    case MenuItemType::TimeZone: mConf.SetSystemTimezone(value).Save(); break;
    case MenuItemType::Keyboard: mConf.SetSystemKbLayout(value).Save(); break;
    case MenuItemType::KodiResolution: if (value.empty()) mConf.DeleteKodiVideoMode().Save(); else mConf.SetKodiVideoMode(value).Save(); break;
    case MenuItemType::SuperGameboyMode: mConf.SetSuperGameBoy(value).Save(); break;
    case MenuItemType::GameRatio: mConf.SetGlobalRatio(value).Save(); break;
    case MenuItemType::ShaderSet: mConf.SetGlobalShaderSet(value).Save(); break;
    case MenuItemType::AdvancedShaders:
    {
      if (value != "none" && (mConf.GetGlobalSmooth()))
        mWindow.pushGui(new GuiMsgBox(mWindow,
                                      _("YOU JUST ACTIVATED THE SHADERS FOR ALL SYSTEMS. FOR A BETTER RENDERING, IT IS ADVISED TO DISABLE GAME SMOOTHING. DO YOU WANT TO CHANGE THIS OPTION AUTOMATICALLY?"),
                                      _("LATER"), nullptr, _("YES"), [this, &item]
                                      { CoItemAsSwitch(item, MenuItemType::Smooth).SetState(false, true); }));
      mConf.SetGlobalShaders(value).Save();
      break;
    }
    case MenuItemType::NetworkPickSSID: if (!value.empty()) CoItemAsEditor(item, MenuItemType::NetworkSSID).SetText(value, true); break;
    case MenuItemType::ThemeRegion: { mConf.SetThemeRegion(value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeTransition: mConf.SetThemeTransition(value).Save(); break;
    case MenuItemType::ThemeOptColorset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeColorSet(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeOptIconset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeIconSet(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeOptMenu: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeMenuSet(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeOptSystemView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeSystemView(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeOptGameView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeGamelistView(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::ThemeOptGameClip: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); mConf.SetThemeGameClipView(s, value).Save(); ThemeManager::Instance().DoThemeChange(&mWindow, false); break; }
    case MenuItemType::BootOnSystem: mConf.SetStartupSelectedSystem(value).Save(); break;
    case MenuItemType::PerSystemEmulator:
    {
      const SystemData& system = *item.Context().System();
      // Split emulator & core
      String emulator;
      String core;
      if (value.Extract(':', emulator, core, false))
      {
        // Set emulator/core
        String defaultEmulator;
        String defaultCore;
        EmulatorManager::GetDefaultEmulator(system, defaultEmulator, defaultEmulator);
        if (emulator == defaultEmulator && core == defaultCore) mConf.SetSystemEmulator(system, "").SetSystemCore(system, "").Save();
        else mConf.SetSystemEmulator(system, emulator).SetSystemCore(system, core).Save();
        // Force refresh of gamelist
        ViewController::Instance().ForceGamelistRefresh((SystemData&)system); // TODO: Add conf notification in ViewController or GamelistView
      }
      else { LOG(LogError) << "[SystemConfigurationGui] Error splitting emulator and core!"; }
      break;
    }
    case MenuItemType::PerSystemRatio: { mConf.SetSystemRatio(*item.Context().System(), value).Save(); break; }
    case MenuItemType::PerSystemShaderSet: { mConf.SetSystemShaderSet(*item.Context().System(), value).Save(); break; }
    case MenuItemType::PerSystemShaders: { mConf.SetSystemShaders(*item.Context().System(), value).Save(); break; }
    case MenuItemType::CrtMenuResolution: { mCrtConf.SetSystemCRTResolution(value).Save(); break; }
    case MenuItemType::CrtSuperrezMultiplier: { mCrtConf.SetSystemCRTSuperrez(value).Save(); break; }
    case MenuItemType::JammaSound: mCrtConf.SetSystemCRTJammaAmpDisable(value == "1").Save(); break;
    case MenuItemType::JammaAmpBoost: mCrtConf.SetSystemCRTJammaMonoAmpBoost(value).Save(); break;
    case MenuItemType::JammaPanelType: mCrtConf.SetSystemCRTJammaPanelButtons(value).Save(); break;
    case MenuItemType::JammaNeogeoLayoutP1: CrtConf::Instance().SetSystemCRTJammaNeogeoLayoutP1(value).Save(); break;
    case MenuItemType::JammaNeogeoLayoutP2: CrtConf::Instance().SetSystemCRTJammaNeogeoLayoutP2(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<String> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperImageType>& item, int id, int index, const ScreenScraperEnums::ScreenScraperImageType& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionImage: mConf.SetScreenScraperMainMedia(value).Save(); break;
    case MenuItemType::ScrapeOptionThumb: mConf.SetScreenScraperThumbnail(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ScreenScraperEnums::ScreenScraperImageType> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperVideoType>& item, int id, int index, const ScreenScraperEnums::ScreenScraperVideoType& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionVideo: mConf.SetScreenScraperVideo(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ScreenScraperEnums::ScreenScraperVideoType> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperRegionPriority>& item, int id, int index, const ScreenScraperEnums::ScreenScraperRegionPriority& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionRegionPriority: mConf.SetScreenScraperRegionPriority(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ScreenScraperRegionPriority> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<Regions::GameRegions>& item, int id, int index, const Regions::GameRegions& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionRegionFavorite: mConf.SetScreenScraperRegion(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<Regions::GameRegions> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<Languages>& item, int id, int index, const Languages& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionLanguageFavorite: mConf.SetScreenScraperLanguage(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<Languages> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScraperNameOptions>& item, int id, int index, const ScraperNameOptions& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeOptionNameFrom: mConf.SetScraperNameOptions(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ScraperNameOptions> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScrapingMethod>& item, int id, int index, const ScrapingMethod& value)
{
  (void)item;
  (void)index;
  (void)value;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeFilter: mScraperData.mMethod = value; break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuMultiChanged<ScrapingMethod*> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ScraperType>& item, int id, int index, const ScraperType& value)
{
  (void)item;
  (void)index;
  (void)value;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeFrom:
    {
      mConf.SetScraperSource(value).Save();
      CoItemAsEditor(item, MenuItemType::ScrapeOptionUsername).SetSelectable(HasScraperCredentials(value));
      CoItemAsEditor(item, MenuItemType::ScrapeOptionPassword).SetSelectable(HasScraperCredentials(value));
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuMultiChanged<ScraperType*> !");
  }
}

void MenuProvider::MenuMultiChanged(const ItemSelectorBase<SystemData*>& item, int id, int index, const std::vector<SystemData*>& value)
{
  (void)item;
  (void)index;
  (void)value;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScrapeSystems: mScraperData.mSystems = value; break;
    case MenuItemType::HideSystems:
    {
      for(SystemData* system : item.AllSelectedValues()) if (!mConf.GetSystemIgnore(*system)) mConf.SetSystemIgnore(*system, true);
      for(SystemData* system : item.AllUnselectedValues()) if (mConf.GetSystemIgnore(*system)) mConf.SetSystemIgnore(*system, false);
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuMultiChanged<vector<SystemData*>> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<RecalboxConf::SoftPatching>& item, int id, int index, const RecalboxConf::SoftPatching& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::SoftPatching: mConf.SetGlobalSoftpatching(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<RecalboxConf::SoftPatching> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<RecalboxConf::Relay>& item, int id, int index, const RecalboxConf::Relay& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::NetplayRelay: mConf.SetNetplayRelay(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<RecalboxConf::Relay> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<RecalboxConf::UpdateType>& item, int id, int index, const RecalboxConf::UpdateType& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::UpdateType: mConf.SetUpdateType(value); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<RecalboxConf::UpdateType> !");
  }
}

void MenuProvider::MenuSliderMoved(int id, float value)
{
  switch((MenuItemType)id)
  {
    case MenuItemType::ScreensaverTimeout: mConf.SetScreenSaverTime((int)value).Save(); break;
    case MenuItemType::PopupHelpDuration: mConf.SetPopupHelp((int)value).Save(); break;
    case MenuItemType::PopupMusic: mConf.SetPopupMusic((int)value).Save(); break;
    case MenuItemType::PopupNetplay: mConf.SetPopupNetplay((int)value).Save(); break;
    case MenuItemType::Brightness: if (mConf.GetBrightness() != (int)value) { Board::Instance().SetBrightness((int) value); mConf.SetBrightness((int) value).Save(); } break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSliderMoved !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<RecalboxConf::Screensaver>& item, int id, int index,
                                     const RecalboxConf::Screensaver& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScreensaverType: mConf.SetScreenSaverType(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<RecalboxConf::Screensaver> !");
  }
}

void MenuProvider::MenuMultiChanged(const ItemSelectorBase<String>& item, int id, int index,
                                    const std::vector<String>& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ScreensaverSystems: mConf.SetScreenSaverSystemList(value).Save(); break;
    case MenuItemType::ArcadeManufacturerSystems: mConf.SetCollectionArcadeManufacturers(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuMultiChanged<String> !");
  }
}

void MenuProvider::ReceiveSyncMessage(const ItemSelectorBase<ThemeSpec>* const& sourceItem)
{
  bool compatiblity = false;
  CheckCompatibility(mLastSelectedTheme.FolderPath, compatiblity, true);

  typedef String (RecalboxConf::*GetThemeOptionMethod)(const String&) const;
  struct ThemeOptionSpecs { MenuItemType type; const char* optionName; GetThemeOptionMethod method; };
  static std::vector<ThemeOptionSpecs> itemToRefresh
  {
    { MenuItemType::ThemeOptColorset  , "colorset",     &RecalboxConf::GetThemeColorSet },
    { MenuItemType::ThemeOptIconset   , "iconset",      &RecalboxConf::GetThemeIconSet },
    { MenuItemType::ThemeOptMenu      , "menu",         &RecalboxConf::GetThemeMenuSet },
    { MenuItemType::ThemeOptSystemView, "systemview",   &RecalboxConf::GetThemeSystemView },
    { MenuItemType::ThemeOptGameView  , "gamelistview", &RecalboxConf::GetThemeGamelistView },
    { MenuItemType::ThemeOptGameClip  , "gameclipview", &RecalboxConf::GetThemeGameClipView },
  };
  String themeName = mConf.GetThemeFolder();
  IniFile::PurgeKey(themeName);
  for(const ThemeOptionSpecs& item2refresh : itemToRefresh)
  {
    SelectorEntry<String>::List list;
    for(const String& s : ThemeManager::Instance().Main().GetSubSetValues(item2refresh.optionName))
      list.push_back({ s, s, false });
    bool empty = list.empty();
    if (empty) list.push_back({ _("OPTION NOT AVAILABLE"), "", false });
    CoItemAsList<String>(*sourceItem, item2refresh.type).ChangeSelectionItems(list, (mConf.*item2refresh.method)(themeName), String::Empty).SetSelectable(!empty);
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ThemeSpec>& item, int id, int index, const ThemeSpec& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::ThemeSet:
    {
      mLastSelectedTheme = value;
      mConf.SetThemeFolder(value.Name).Save();
      mSender.SendDelayed(&item, 2);
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ThemeSpec> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<SystemSorting>& item, int id, int index,
                                     const SystemSorting& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::SystemSorting:
    {
      mConf.SetSystemSorting(value).Save();
      // TODO move the following code in ViewController in OnSystemSortingChanged() notification
      mSystemManager.SystemSorting();
      ViewController::Instance().getSystemListView().Sort();
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<SystemSorting> !");
  }
}

void MenuProvider::MenuMultiChanged(const ItemSelectorBase<GameGenres>& item, int id, int index,
                                    const std::vector<GameGenres>& value)
{
  (void)id;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::VirtualPerGenre:
    {
      // Build list & save
      String::List list;
      for (GameGenres gg: value) list.push_back(SystemManager::BuildGenreSystemName(gg));
      mConf.SetCollectionGenre(list);

      // Refresh systems
      for (GameGenres ug: item.AllUnselectedValues())
        mSystemManager.UpdateVirtualGenreSystemsVisibility(ug, SystemManager::Visibility::Hide);
      for (GameGenres sg: item.AllSelectedValues())
        mSystemManager.UpdateVirtualGenreSystemsVisibility(sg, SystemManager::Visibility::ShowAndSelect);
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuMultiChanged<GameGenres> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<Overclocking::Overclock>& item, int id, int index,
                                     const Overclocking::Overclock& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::Overclock:
    {
      if (value.Hazardous())
        mWindow.pushGui(new GuiMsgBox(mWindow, _("TURBO AND EXTREM OVERCLOCK PRESETS MAY CAUSE SYSTEM UNSTABILITIES, SO USE THEM AT YOUR OWN RISK.\nIF YOU CONTINUE, THE SYSTEM WILL REBOOT NOW."),
                        _("YES"), [this, value] { Overclocking().Install(value); MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); }, _("NO"), nullptr));
      else { Overclocking().Install(value); MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); }
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<Overclocking::Overclock> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<CrtAdapterType>& item, int id, int index,
                                     const CrtAdapterType& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::CrtAdapter:
    {
      CrtAdapterType oldValue = Board::Instance().CrtBoard().GetCrtAdapter();
      if (value == CrtAdapterType::None && oldValue != CrtAdapterType::None)
      {
        CrtConf::Instance().SetSystemCRT(CrtAdapterType::None).Save();
        mConf.SetEmulationstationVideoMode("default").Save();
        CoItemAsList<String>(item, MenuItemType::CrtMenuResolution).SetSelectedItemValue("default", true);
      }
      CrtConf::Instance().SetSystemCRT(value).Save();
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<CrtAdapterType> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<ICrtInterface::HorizontalFrequency>& item, int id, int index, const ICrtInterface::HorizontalFrequency &value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::CrtScreenType:
    {
      mWindow.pushGui(new GuiMsgBox(mWindow,
                                    _("Make sure to check the compatibility of your hardware before changing the recalbox refresh rate. Are you sure you want to switch the display mode to ").Append(ICrtInterface::HorizontalFrequencyToHumanReadable(value)).Append("?"),
                                    _("CANCEL"), [&item] { item.SetSelectedItemValue(Board::Instance().CrtBoard().GetHorizontalFrequency(), false); },
                                    _("YES"), [value]{ CrtConf::Instance().SetSystemCRTScreenType((int)value).Save(); }));
      break;
    }
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<ICrtInterface::HorizontalFrequency> !");
  }
}

void MenuProvider::MenuSingleChanged(ItemSelectorBase<CrtScanlines>& item, int id, int index, const CrtScanlines& value)
{
  (void)item;
  (void)index;
  switch((MenuItemType)id)
  {
    case MenuItemType::CrtScanline240in480: mCrtConf.SetSystemCRTScanlines31kHz(value).Save(); break;
    default:
      mWindow.displayMessage("[MenuProcessor] Unprocessed id " + MenuConverters::ItemToString((MenuItemType)id) + " in MenuSingleChanged<CrtScanlines> !");
  }
}
