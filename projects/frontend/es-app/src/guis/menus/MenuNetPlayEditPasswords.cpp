#include <RecalboxConf.h>
#include "guis/menus/MenuNetPlayEditPasswords.h"

MenuNetPlayEditPasswords::MenuNetPlayEditPasswords(WindowManager& window)
  : Menu(window, InheritableContext(), _("PREDEFINED PASSWORDS"))
{
}

void MenuNetPlayEditPasswords::BuildMenuItems()
{
  for(int i = 0; i < DefaultPasswords::sPasswordCount; i++)
  {
    String password = RecalboxConf::Instance().GetNetplayPasswords(i);
    if (password.empty()) password = DefaultPasswords::sDefaultPassword[i];
    AddEditable(_("PASSWORD #%i").Replace("%i", String(i)), password, i, this, false);
  }

  AddAction(_("VALIDATE"), "", 0, true, this, String::Empty);
}

void MenuNetPlayEditPasswords::MenuEditableChanged(ItemEditable& item, int id, const String& newText)
{
  (void)item;
  if (!newText.empty())
    RecalboxConf::Instance().SetNetplayPasswords(id, newText).Save();
}
