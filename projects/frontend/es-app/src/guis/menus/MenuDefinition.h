//
// Created by bkg2k on 23/10/24.
//
#pragma once

#include "utils/String.h"
#include "themes/MenuThemeData.h"
#include "MenuItemType.h"
#include "utils/eval/SimpleExpressionEvaluator.h"
#include "utils/locale/LocaleHelper.h"

// Single item definition
class ItemDefinition
{
  public:
    enum class ItemCategory
    {
      Item,
      SubMenu,
      Header
    };

  private:
    const String mCaption;                //!< Item/SubMenu/Header caption
    const String mCaptionIf;              //!< Conditional override captions
    const String mHelp;                   //!< Item/SubMenu regular help
    const String mUnselectableHelp;       //!< Item/SubMenu unselectable help
    const String mCondition;              //!< Dynamic condition
    MenuThemeData::MenuIcons::Type mIcon; //!< Icon
    const ItemCategory mType;             //!< Type
    const MenuItemType mItemType;         //!< Item type
    const MenuContainerType mMenuType;    //!< Submenu identifier
    bool mReboot;                         //!< Must reboot on item change ?
    bool mRelaunch;                       //!< Must relaunch on item change ?
    bool mBootConf;                       //!< Changing this item requires to backup the conf in /boot ?

  public:
    /*!
     * @brief Header constructor
     * @param caption header caption
     */
    explicit ItemDefinition(const String& caption)
      : mCaption(caption)
      , mIcon(MenuThemeData::MenuIcons::Type::Unknown)
      , mType(ItemCategory::Header)
      , mItemType(MenuItemType::_Error_)
      , mMenuType(MenuContainerType::_Error_)
      , mReboot(false)
      , mRelaunch(false)
      , mBootConf(false)
    {}

    /*!
     * @brief Item constructor
     * @param itemType Item type
     * @param icon Icon name - empty for no icon
     * @param caption Item caption
     * @param help Regular help string
     * @param unselectableHelp Help string when the item is grayed
     * @param condition Dynamic condition for the item to appear in the menu
     */
    ItemDefinition(const MenuItemType itemType, MenuThemeData::MenuIcons::Type icon, const String& caption, const String& altCaptionIf, const String& help, const String& unselectableHelp, const String& condition, bool relaunch, bool reboot, bool bootConf)
      : mCaption(caption)
      , mCaptionIf(altCaptionIf)
      , mHelp(help)
      , mUnselectableHelp(unselectableHelp)
      , mCondition(condition)
      , mIcon(icon)
      , mType(ItemCategory::Item)
      , mItemType(itemType)
      , mMenuType(MenuContainerType::_Error_)
      , mReboot(reboot)
      , mRelaunch(relaunch)
      , mBootConf(bootConf)
    {}

    /*!
     * @brief Submenu constructor
     * @param menu Menu identifier
     * @param icon Icon name- empty fo rno icon
     * @param caption Menu caption
     * @param help Regular help string
     * @param unselectableHelp Help string when the item is grayed
     * @param condition Dynamic condition for the submenu to appear in the menu
     */
    ItemDefinition(const MenuContainerType& menu, MenuThemeData::MenuIcons::Type icon, const String& caption, const String& altCaptionIf, const String& help, const String& unselectableHelp, const String& condition)
      : mCaption(caption)
      , mCaptionIf(altCaptionIf)
      , mHelp(help)
      , mUnselectableHelp(unselectableHelp)
      , mCondition(condition)
      , mIcon(icon)
      , mType(ItemCategory::SubMenu)
      , mItemType(MenuItemType::_Error_)
      , mMenuType(menu)
      , mReboot(false)
      , mRelaunch(false)
      , mBootConf(false)
    {}

    /*
     * Accessors
     */

    [[nodiscard]] ItemCategory Category() const { return mType; }
    [[nodiscard]] MenuItemType Type() const { return mItemType; }
    [[nodiscard]] String Help() const { return _S(mHelp); }
    [[nodiscard]] String UnselectableHelp() const { return _S(mUnselectableHelp); }
    [[nodiscard]] MenuContainerType MenuType() const { return mMenuType; }
    [[nodiscard]] String Condition() const { return mCondition; }
    [[nodiscard]] bool HasCondition() const { return !mCondition.empty(); }
    [[nodiscard]] MenuThemeData::MenuIcons::Type Icon() const { return mIcon; }
    [[nodiscard]] bool Reboot() const { return mReboot; }
    [[nodiscard]] bool Relaunch() const { return mRelaunch; }
    [[nodiscard]] bool BootConf() const { return mBootConf; }
    [[nodiscard]] const String& RawCaption() const { return mCaption; }
    [[nodiscard]] String Caption(SimpleExpressionEvaluator::IIdentifierEvaluator* evaluator) const
    {
      if (!mCaptionIf.empty())
      {
        String condition;
        String caption;
        for(String& subCondition : mCaptionIf.Split('|'))
          if (subCondition.Extract(';', condition, caption, true))
            if (SimpleExpressionEvaluator(*evaluator).Evaluate(condition))
              return _S(caption);
      }
      // No previous condition was true, take the caption
      return _S(mCaption);
    }
};

// Single menu structure
struct MenuDefinition
{
  const std::vector<ItemDefinition> mItems;           //!< Item definitions
  const String mCaption;                              //!< Menu caption
  const String mCaptionIf;                            //!< Conditional override captions
  const String mHelp;                                 //!< Item/SubMenu regular help
  const String mUnselectableHelp;                     //!< Item/SubMenu unselectable help
  const String mCondition;                            //!< Dynamic condition
  const enum class Footer { Recalbox, Game } mFooter; //!< Footer type
  MenuThemeData::MenuIcons::Type mIcon;               //!< Icon
  const MenuContainerType mType;                      //!< Menu type
  const bool mAnimated;                               //!< Menu animation ?

  MenuDefinition(MenuContainerType type, MenuThemeData::MenuIcons::Type icon, const String& caption, const String& altCaptionIf, Footer footer, std::vector<ItemDefinition>&& items,
                 const String& help, const String& unselectableHelp, const String& condition, bool animated)
    : mItems(items)
    , mCaption(caption)
    , mCaptionIf(altCaptionIf)
    , mHelp(help)
    , mUnselectableHelp(unselectableHelp)
    , mCondition(condition)
    , mFooter(footer)
    , mIcon(icon)
    , mType(type)
    , mAnimated(animated)
  {}
};

