//
// Created by bkg2k on 21/10/24.
//

#include "MenuBuilder.h"
#include "Upgrade.h"
#include "MenuProvider.h"
#include "guis/GuiMsgBox.h"
#include "guis/GuiInfoPopup.h"
#include "MainRunner.h"
#include "guis/menus/tasks/MenuTaskRefreshConnection.h"
#include "guis/menus/tasks/MenuTaskRefreshIp.h"
#include "guis/menus/tasks/MenuTaskRefreshSsid.h"
#include "guis/menus/vault/MenuThemeOptions.h"
#include <guis/menus/base/ItemText.h>
#include <guis/menus/base/ItemSelector.h>
#include <guis/menus/base/ItemSlider.h>
#include <guis/menus/base/ItemBar.h>
#include <guis/menus/base/ItemSwitch.h>
#include <guis/menus/base/ItemAction.h>
#include <guis/menus/base/ItemEditable.h>
#include <guis/menus/base/ItemSubMenu.h>
#include <systems/SystemManager.h>
#include <patreon/PatronInfo.h>

MenuBuilder::~MenuBuilder()
{
  bool reboot = false;
  bool relaunch = false;
  bool bootConf = false;

  // Check reboot/relaunch
  for(const auto& link : mItemsLinks)
    if (link.first->HasChanged())
    {
      reboot |= link.second->Reboot();
      relaunch |= link.second->Relaunch();
      bootConf |= link.second->BootConf();
    }

  if (reboot)
  {
    mWindow.pushGui(new GuiMsgBox(mWindow, _("THE SYSTEM WILL NOW REBOOT"),
                                  _("OK"), []{ MainRunner::RequestQuit(MainRunner::ExitState::NormalReboot); },
                                  _("LATER"), std::bind(MenuBuilder::RebootPending, &mWindow)));
  }
  else if (relaunch)
  {
    mWindow.pushGui(
      new GuiMsgBox(mWindow, _("Recalbox interface must restart to apply your changes."),
                    _("OK"), []{ MainRunner::RequestQuit(MainRunner::ExitState::Relaunch, true); },
                    _("LATER"), std::bind(MenuBuilder::RebootPending, &mWindow)));
  }

  if (bootConf)
  {
    RecalboxSystem::MakeBootReadWrite();
    Files::CopyFile(mConf.FilePath(), Path("/boot/recalbox-backup.conf"));
    RecalboxSystem::MakeBootReadOnly();
  }

  // Stop background tasks
  for(IMenuBackgroundTask* task : mTasks)
  {
    task->TaskStop();
    delete task;
  }
}

void MenuBuilder::RebootPending(WindowManager* window)
{
  static bool pending = false;
  if (!pending)
  {
    window->InfoPopupAdd(new GuiInfoPopup(*window, _("A reboot is required to apply pending changes."), 10000, PopupType::Reboot));
    pending = true;
  }
}

void MenuBuilder::BuildThemeOptionSelector(const ItemDefinition& item, const String& selected, const String::List& items)
{
  bool found = false;
  String realSelected;
  for(const String& s : items) if (s == selected) { found = true; realSelected = s; break; }
  if (!found && !items.empty()) realSelected = items.front();

  // Build list
  SelectorEntry<String>::List list;
  for (const String& s : items) list.push_back({ s, s, s == realSelected });
  // Try numeric sorting, else  use an alphanumeric sort
  if (!TrySortNumerically(list))
    std::sort(list.begin(), list.end(), [] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return a.mText < b.mText; });

  ItemSelector<String>* existing = AsList<String>(item.Type());
  if (!items.empty())
  {
    if (existing != nullptr) existing->ChangeSelectionItems(list, realSelected, list.front().mValue).SetSelectable(true);
    else AddList<String>(item, list, realSelected, list.front().mValue, false);
  }
  else
  {
    if (existing != nullptr) existing->ChangeSelectionItems({{ _("OPTION NOT AVAILABLE"), "" }}, "", "").SetSelectable(false);
    else AddList<String>(item, {{ _("OPTION NOT AVAILABLE"), "" }}, "", "", true);
  }
}

bool MenuBuilder::TrySortNumerically(SelectorEntry<String>::List& list)
{
  HashMap<String, int> nameToNumeric;
  for(const SelectorEntry<String>& item : list)
  {
    if (item.mText.empty()) return false;
    size_t pos = item.mText.find_first_not_of("0123456789");
    if (pos == 0) return false;
    nameToNumeric[item.mText] = (pos == std::string::npos) ? item.mText.AsInt() : item.mText.AsInt(item.mText[pos]);
  }

  std::sort(list.begin(), list.end(), [&nameToNumeric] (const SelectorEntry<String>& a, const SelectorEntry<String>& b) -> bool { return nameToNumeric[a.mText] < nameToNumeric[b.mText]; });
  return true;
}

bool MenuBuilder::EvaluateIdentifier(const String& identifier)
{
  bool isBeta =
    #if defined(BETA) || defined(DEBUG)
    true
  #else
    false
  #endif
    ;

  if (identifier == "crt"         ) return mResolver.HasCrt();
  if (identifier == "jamma"       ) return mResolver.HasJamma();
  if (identifier == "rrgbd"       ) return mResolver.HasRRGBD();
  if (identifier == "overscan"    ) return mResolver.HasCrt() && !mResolver.HasJamma();
  if (identifier == "tate"        ) return mResolver.IsTate();
  if (identifier == "tateright"   ) return mResolver.IsTateRight();
  if (identifier == "tateleft"    ) return mResolver.IsTateLeft();
  if (identifier == "qvga"        ) return mResolver.IsQVGA();
  if (identifier == "vga"         ) return mResolver.IsVGA();
  if (identifier == "hd"          ) return mResolver.IsHD();
  if (identifier == "fhd"         ) return mResolver.IsFHD();
  if (identifier == "virtual"     ) return Context().HasSystem() ? Context().System()->IsVirtual() : false;
  if (identifier == "arcade"      ) return Context().HasSystem() ? Context().System()->IsArcade() : false;
  if (identifier == "port"        ) return Context().HasSystem() ? Context().System()->IsPorts() : false;
  if (identifier == "console"     ) return Context().HasSystem() ? Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Console : false;
  if (identifier == "handheld"    ) return Context().HasSystem() ? Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Handheld : false;
  if (identifier == "computer"    ) return Context().HasSystem() ? Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Computer : false;
  if (identifier == "fantasy"     ) return Context().HasSystem() ? Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Fantasy : false;
  if (identifier == "engine"      ) return Context().HasSystem() ? Context().System()->Descriptor().Type() == SystemDescriptor::SystemType::Engine : false;
  if (identifier == "favorite"    ) return Context().HasSystem() ? Context().System()->IsFavorite() : false;
  if (identifier == "lastplayed"  ) return Context().HasSystem() ? Context().System()->IsLastPlayed() : false;
  if (identifier == "bartop"      ) return mResolver.IsBartopModeOrHigher();
  if (identifier == "nomenu"      ) return mResolver.IsNoMenuMode();
  // All direct accesses below MUST be moved into the global resolver interface
  if (identifier == "kodi"        ) return mConf.GetKodiEnabled();
  if (identifier == "kodiexists"  ) return RecalboxSystem::kodiExists();
  if (identifier == "shutdownmenu") return Case::CurrentCase().CanShutdownFromMenu();
  if (identifier == "extradevices") return mProvider.SystemManager().GetMountMonitor().AllMountPoints().size() > 1;
  if (identifier == "patron"      ) return PatronInfo::Instance().IsPatron();
  if (identifier == "beta"        ) return isBeta;
  if (identifier == "pcboard"     ) return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::PCx64 || t == BoardType::PCx86; });
  if (identifier == "pi02board"   ) return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi02; });
  if (identifier == "pi3board"    ) return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi3 || t == BoardType::Pi3plus; });
  if (identifier == "pi4board"    ) return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi4 || t == BoardType::Pi400; });
  if (identifier == "pi5board"    ) return ({ BoardType t = Board::Instance().GetBoardType(); t == BoardType::Pi5; });
  if (identifier == "vulkan"      ) return Board::Instance().HasVulkanSupport();
  if (identifier == "brightness"  ) return Board::Instance().HasBrightnessSupport();
  if (identifier == "handheld"    ) return Board::Instance().IsHandheldSystem();
  if (identifier == "homesystem"  ) return Board::Instance().IsHomeSystem();

  { LOGT(LogDebug) << "[Theme] Unknown identifier " << identifier << ". False evaluation assumed."; }
  return false;
}

void MenuBuilder::BuildMenuItems()
{
  for(const ItemDefinition& item : mDefinition.mItems)
  {
    // Add item
    switch (item.Category())
    {
      case ItemDefinition::ItemCategory::SubMenu:
      {
        // Condition?
        if (item.HasCondition() && !SimpleExpressionEvaluator(*this).Evaluate(item.Condition())) break;
        // Custom create?
        if (!AddCustomSubMenu(item))
        {
          ItemSubMenu* submenu = (item.Icon() != MenuThemeData::MenuIcons::Type::Unknown) ?
                                 Menu::AddSubMenu(item.Caption(this), item.Icon(), (int)item.MenuType(), &mProvider, item.Help(), mProvider.IsMenuUnselectable(item.MenuType()), item.UnselectableHelp()) :
                                 Menu::AddSubMenu(item.Caption(this), (int)item.MenuType(), &mProvider, item.Help(), mProvider.IsMenuUnselectable(item.MenuType()), item.UnselectableHelp());
          // Merge context
          submenu->MergeContext(Context());
        }
        break;
      }
      case ItemDefinition::ItemCategory::Item:
      {
        // Condition?
        if (item.HasCondition())
          if (!SimpleExpressionEvaluator(*this).Evaluate(item.Condition()))
            break;
        AddItem(item);
        break;
      }
      case ItemDefinition::ItemCategory::Header:
      {
        AddHeader(item.Caption(this));
        break;
      }
      default:
      { LOG(LogFatal) << "[MenuProvider] Unknown menu item !"; }
    }
  }

  if (Count() == 0)
    Menu::AddText("EMPTY MENU", "CHECK MENU.XML!");
  else
    for(IMenuBackgroundTask* task : mTasks)
      task->TaskStart();
}

void MenuBuilder::MenuItemShow(ItemBase& item)
{
  // Refresh system option submenu
  if (item.IsSubMenu() && (MenuContainerType)item.Identifier() == MenuContainerType::AdvancedEmulatorOptionsList)
    item.ChangeLabel(mProvider.SystemNameWithEmulator(item.Context().System()));
}

bool MenuBuilder::AddCustomSubMenu(const ItemDefinition& subMenuItem)
{
  switch(subMenuItem.MenuType())
  {
    case MenuContainerType::StorageDevice:
    {
      for(const StorageDevices::Device& device : mProvider.StorageDevices().GetStorageDevices())
      {
        String p1;
        switch(device.Type)
        {
          case StorageDevices::Types::Internal: continue;
          case StorageDevices::Types::Ram: continue;
          case StorageDevices::Types::Network: continue;
          case StorageDevices::Types::External: p1 = device.DisplayableName(); break;
        }
        AddSubMenu(subMenuItem, false)->MergeContext(Context())
                                      .MergeContext(InheritableContext(&device))
                                      .ReplaceParameters(p1);
      }
      return true;
    }
    case MenuContainerType::AdvancedEmulatorOptionsList:
    {
      for(SystemData* system : mProvider.SystemManager().VisibleSystemList())
        AddSubMenu(subMenuItem, false)->MergeContext(Context())
                                      .MergeContext(InheritableContext(system))
                                      .ReplaceParameters(mProvider.SystemNameWithEmulator(system));
      return true;
    }
    case MenuContainerType::Main:
    case MenuContainerType::SystemSettings:
    case MenuContainerType::StorageList:
    case MenuContainerType::Update:
    case MenuContainerType::Controllers:
    case MenuContainerType::Crt:
    case MenuContainerType::UI:
    case MenuContainerType::Screensaver:
    case MenuContainerType::Theme:
    case MenuContainerType::PopupSettings:
    case MenuContainerType::GameFilters:
    case MenuContainerType::Lists:
    case MenuContainerType::InGame:
    case MenuContainerType::NetplayPasswords:
    case MenuContainerType::Arcade:
    case MenuContainerType::Tate:
    case MenuContainerType::Advanced:
    case MenuContainerType::BootSettings:
    case MenuContainerType::VirtualSystems:
    case MenuContainerType::Resolutions:
    case MenuContainerType::AdvancedEmulatorOptions:
    case MenuContainerType::Kodi:
    case MenuContainerType::Sound:
    case MenuContainerType::Network:
    case MenuContainerType::Scraper:
    case MenuContainerType::Download:
    case MenuContainerType::Gamelist:
    case MenuContainerType::Quit:
    case MenuContainerType::ChooseKodiLobby: break;
    case MenuContainerType::_Error_: { LOG(LogFatal) << "[MenuBuilder] Pretty impossible to see this eror :)"; }
  }
  return false;
}

void MenuBuilder::AddItem(const ItemDefinition& item)
{
  bool isBeta =
    #if defined(BETA) || defined(DEBUG)
    true
    #else
    false
  #endif
  ;

  switch(item.Type())
  {
    // Version & platform
    case MenuItemType::RecalboxVersion: { AddText(item, mProvider.GetVersionString()); break; }
    case MenuItemType::UsedShare:
    {
      const StorageDevices::Device& share = mProvider.StorageDevices().GetShareDevices().front();
      String text = _S((_F("{0} free of {1}") / share.HumanFree() / share.HumanSize())());
      AddBar(item, text, (float)share.Free / (float)share.Size, false);
      break;
    }
    case MenuItemType::ShareDevice:
    {
      // Storage device
      int currentIndex = 0;
      SelectorEntry<StorageDevices::Device>::List list = mProvider.GetStorageEntries(currentIndex);
      if (!list.empty())
        AddList<StorageDevices::Device>(item, list, list[currentIndex].mValue, list[currentIndex].mValue, false);
      break;
    }
    case MenuItemType::DeviceList: break;
    case MenuItemType::Language: { AddList<String>(item, mProvider.GetCultureEntries(), mConf.GetSystemLanguage(), "en_US", false); break; }
    case MenuItemType::TimeZone: { AddList<String>(item, mProvider.GetTimeZones(), mConf.GetSystemLanguage(), "UTC", false); break; }
    case MenuItemType::Keyboard: { AddList<String>(item, mProvider.GetKeyboardEntries(), mConf.GetSystemKbLayout(), "us", false); break; }
    case MenuItemType::Shutdown: /* fallthrough to Restart */
    case MenuItemType::FastShutdown: /* fallthrough to Restart */
    case MenuItemType::Restart: { AddAction(item, "", true, false); break; }
    case MenuItemType::RunKodi: { AddAction(item, "RUN", true, false); break; }
    case MenuItemType::OpenLobby: { AddAction(item, "OPEN", true, false); break; }
    case MenuItemType::EnableKodi: { AddSwitch(item, mConf.GetKodiEnabled(), false); break; }
    case MenuItemType::KodiResolution: { AddList<String>(item, mProvider.GetKodiResolutionsEntries(), !mConf.IsDefinedKodiVideoMode() ? String::Empty : mConf.GetKodiVideoMode(), String::Empty, false); break; }
    case MenuItemType::KodiOnStartup: { AddSwitch(item, mConf.GetKodiAtStartup(), false); break; }
    case MenuItemType::KodiOnX: { AddSwitch(item, mConf.GetKodiXButton(), false); break; }
    case MenuItemType::CheckBios: { AddAction(item, "CHECK", true, false); break; }
    case MenuItemType::ShowLicense: { AddAction(item, "SHOW", true, false); break; }
    case MenuItemType::ScrapeFrom: { AddList<ScraperType>(item, mProvider.GetScrapersEntries(), mConf.GetScraperSource(), ScraperType::ScreenScraper, false); break; }
    case MenuItemType::ScrapeAuto: { AddSwitch(item, mConf.GetScraperAuto(), false); break; }
    case MenuItemType::ScrapeOptionNameFrom: { AddList<ScraperNameOptions>(item, mProvider.GetScraperNameOptionsEntries(), mConf.GetScraperNameOptions(), ScraperNameOptions::GetFromScraper, false); break; }
    case MenuItemType::ScrapeOptionImage: { AddList<ScreenScraperEnums::ScreenScraperImageType>(item, mProvider.GetScraperImagesEntries(), mConf.GetScreenScraperMainMedia(), ScreenScraperEnums::ScreenScraperImageType::MixV2, false); break; }
    case MenuItemType::ScrapeOptionVideo: { AddList<ScreenScraperEnums::ScreenScraperVideoType>(item, mProvider.GetScraperVideosEntries(), mConf.GetScreenScraperVideo(), ScreenScraperEnums::ScreenScraperVideoType::None, false); break; }
    case MenuItemType::ScrapeOptionThumb: { AddList<ScreenScraperEnums::ScreenScraperImageType>(item, mProvider.GetScraperThumbnailsEntries(), mConf.GetScreenScraperThumbnail(), ScreenScraperEnums::ScreenScraperImageType::None, false); break; }
    case MenuItemType::ScrapeOptionRegionPriority: { AddList<ScreenScraperEnums::ScreenScraperRegionPriority>(item, mProvider.GetScraperRegionOptionsEntries(), mConf.GetScreenScraperRegionPriority(), ScreenScraperEnums::ScreenScraperRegionPriority::DetectedRegion, false); break; }
    case MenuItemType::ScrapeOptionRegionFavorite: { AddList<Regions::GameRegions>(item, mProvider.GetScraperRegionsEntries(), mConf.GetScreenScraperRegion(), Regions::GameRegions::WOR, false); break; }
    case MenuItemType::ScrapeOptionLanguageFavorite: { AddList<Languages>(item, mProvider.GetScraperLanguagesEntries(), LanguagesTools::GetScrapingLanguage(), Languages::EN, false); break; }
    case MenuItemType::ScrapeOptionManual: { AddSwitch(item, mConf.GetScreenScraperWantManual(), false); break; }
    case MenuItemType::ScrapeOptionMap: { AddSwitch(item, mConf.GetScreenScraperWantMaps(), false); break; }
    case MenuItemType::ScrapeOptionP2K: { AddSwitch(item, mConf.GetScreenScraperWantP2K(), false); break; }
    case MenuItemType::ScrapeOptionUsername:
    {
      if (!mItemsTypes.contains(MenuItemType::ScrapeFrom)) { LOG(LogError) << "[MenuBuilder] MenuItemType::ScrapeOptionUsername must be created AFTER MenuItemType::ScrapeFrom."; }
      ScraperType type = mItemsTypes[MenuItemType::ScrapeFrom]->AsList<ScraperType>()->SelectedValue();
      AddEditor(item, mProvider.GetScraperLogin(type), false, mProvider.HasScraperCredentials(type));
      break;
    }
    case MenuItemType::ScrapeOptionPassword:
    {
      if (!mItemsTypes.contains(MenuItemType::ScrapeFrom)) { LOG(LogError) << "[MenuBuilder] MenuItemType::ScrapeOptionPassword must be created AFTER MenuItemType::ScrapeFrom."; }
      ScraperType type = mItemsTypes[MenuItemType::ScrapeFrom]->AsList<ScraperType>()->SelectedValue();
      AddEditor(item, mProvider.GetScraperPassword(type), false, mProvider.HasScraperCredentials(type));
      break;
    }
    case MenuItemType::ScrapeFilter: { AddList<ScrapingMethod>(item, mProvider.GetScrapingMethods(), mProvider.ScraperData().mMethod, ScrapingMethod::AllIfNothingExists, false); break; }
    case MenuItemType::ScrapeSystems: { AddMultiList<SystemData*>(item, mProvider.GetScrapableSystemsEntries(), false); break; }
    case MenuItemType::ScrapeRun: { AddAction(item, "RUN", true, false); break; }
    case MenuItemType::Rewind: { AddSwitch(item, mConf.GetGlobalRewind(), false); break; }
    case MenuItemType::SoftPatching: { AddList<RecalboxConf::SoftPatching>(item, mProvider.GetSoftpatchingEntries(), mConf.GetGlobalSoftpatching(), RecalboxConf::SoftPatching::Disable, false); break; }
    case MenuItemType::ShowSaveStates: { AddSwitch(item, mConf.GetGlobalAutoSave(), false); break; }
    case MenuItemType::AutoSave: { AddSwitch(item, mConf.GetGlobalAutoSave(), false); break; }
    case MenuItemType::PressTwice: { AddSwitch(item, mConf.GetGlobalQuitTwice(), false); break; }
    case MenuItemType::SuperGameboyMode: { AddList<String>(item, mProvider.GetSuperGameBoyEntries(), mConf.GetSuperGameBoy(), "gb", false); break; }
    case MenuItemType::GameRatio: { AddList<String>(item, mProvider.GetRatioEntries(), mConf.GetGlobalRatio(), "auto", false); break; }
    case MenuItemType::RecalboxOverlays: { AddSwitch(item, mConf.GetGlobalRecalboxOverlays(), false); break; }
    case MenuItemType::Smooth: { AddSwitch(item, mConf.GetGlobalSmooth(), false); break;}
    case MenuItemType::IntegerScale:
    case MenuItemType::IntegerScaleCrt: { AddSwitch(item, mConf.GetGlobalIntegerScale(), false); break; }
    case MenuItemType::ShaderSet: { AddList<String>(item, mProvider.GetShaderPresetsEntries(), mConf.GetGlobalShaderSet(), "none", false); break; }
    case MenuItemType::AdvancedShaders:{ AddList<String>(item, mProvider.GetShadersEntries(), mConf.GetGlobalShaders(), "", false); break; }
    case MenuItemType::HDMode:{ AddSwitch(item, mConf.GetGlobalHDMode(), false); break; }
    case MenuItemType::WideScreen: { AddSwitch(item, mConf.GetGlobalWidescreenMode(), false); break; }
    case MenuItemType::VulkanDriver: { AddSwitch(item, mConf.GetGlobalVulkanDriver(), false); break; }
    case MenuItemType::Retroachievements: { AddSwitch(item, mConf.GetRetroAchievementOnOff(), false); break; }
    case MenuItemType::RetroachievementsHardcore: { AddSwitch(item, mConf.GetRetroAchievementHardcore(), false); break; }
    case MenuItemType::RetroachievementsUsername: { AddEditor(item, mConf.GetRetroAchievementLogin(), false, false); break; }
    case MenuItemType::RetroachievementsPassword: { AddEditor(item, mConf.GetRetroAchievementPassword(), true, false); break; }
    case MenuItemType::Netplay: { AddSwitch(item, mConf.GetNetplayEnabled(), false); break; }
    case MenuItemType::NetplayNickname: { AddEditor(item, mConf.GetNetplayLogin(), false, false); break; }
    case MenuItemType::NetplayPort: { AddEditor(item, String(mConf.GetNetplayPort()), false, false); break; }
    case MenuItemType::NetplayRelay: { AddList<RecalboxConf::Relay>(item, mProvider.GetMitmEntries(), mConf.GetNetplayRelay(), RecalboxConf::Relay::None, false); break; }
    case MenuItemType::HashRoms: { AddAction(item, "RUN", true, false); break; }
    case MenuItemType::UpdateCheck: { AddSwitch(item, mConf.GetUpdatesEnabled(), false); break; }
    case MenuItemType::UpdateAvailable: { AddText(item, Upgrade::Instance().PendingUpdate() ? _("YES").Append(" (").Append(Upgrade::Instance().NewVersion()).Append(')') : _("NO")); break; }
    case MenuItemType::UpdateChangelog: { AddAction(item, "SHOW", true, !Upgrade::Instance().PendingUpdate()); break; }
    case MenuItemType::UpdateStart: { AddAction(item, "GO!", true, !Upgrade::Instance().PendingUpdate()); break; }
    case MenuItemType::UpdateType: { if (isBeta || PatronInfo::Instance().IsPatron()) AddList<RecalboxConf::UpdateType>(item, mProvider.GetUpdateTypeEntries(), mConf.GetUpdateType(), RecalboxConf::UpdateType::Stable, false); break; }
    case MenuItemType::UpdateCheckNow: { AddAction(item, _("CHECK"), true, false); break; }
    case MenuItemType::NetworkStatus: { AddTask(new MenuTaskRefreshConnection(*AddText(item, _("NOT CONNECTED")))); break; }
    case MenuItemType::NetworkIP: { AddTask(new MenuTaskRefreshIP(*AddText(item, _("UNAVAILABLE")))); break; }
    case MenuItemType::NetworkHostname: { AddEditor(item, mConf.GetHostname(), false, false); break; }
    case MenuItemType::NetworkEnableWIFI: { AddSwitch(item, mConf.GetWifiEnabled(), false); break; }
    case MenuItemType::NetworkPickSSID: { AddTask(new MenuTaskRefreshSSID(*AddList<String>(item, { { _("NO WIFI ACCESS POINT"), String::Empty } }, String::Empty, String::Empty, false))); break; }
    case MenuItemType::NetworkSSID: { AddEditor(item, mConf.GetWifiSSID(), false, false); break; }
    case MenuItemType::NetworkPassword: { AddEditor(item, mConf.GetWifiKey(), true, false); break; }
    case MenuItemType::NetworkWPS: { AddAction(item, _("CONNECT"), true, false); break; }
    case MenuItemType::ShowOnlyLastVersions: { AddSwitch(item, mConf.GetShowOnlyLatestVersion(), false); break; }
    case MenuItemType::ShowOnlyFavorites: { AddSwitch(item, mConf.GetFavoritesOnly(), false); break; }
    case MenuItemType::ShowFavoriteFirst: { AddSwitch(item, mConf.GetFavoritesFirst(), false); break; }
    case MenuItemType::ShowHiddenGames: { AddSwitch(item, mConf.GetShowHidden(), false); break; }
    case MenuItemType::ShowMahjongCasino: { AddSwitch(item, !mConf.GetHideBoardGames(), false); break; }
    case MenuItemType::ShowAdultGames: { AddSwitch(item, !mConf.GetFilterAdultGames(), false); break; }
    case MenuItemType::ShowPreinstalledGames:  { AddSwitch(item, !mConf.GetGlobalHidePreinstalled(), false); break; }
    case MenuItemType::Show3PlayerGames: { AddSwitch(item, mConf.GetShowOnly3PlusPlayers(), false); break; }
    case MenuItemType::ShowOnlyYoko: { AddSwitch(item, mConf.GetShowOnlyYokoGames(), false); break; }
    case MenuItemType::ShowOnlyTate: { AddSwitch(item, mConf.GetShowOnlyTateGames(), false); break; }
    case MenuItemType::ShowNonGames: { AddSwitch(item, !mConf.GetHideNoGames(), false); break; }
    case MenuItemType::ScreensaverTimeout: { AddSlider(item, 0.f, 30.f, 1.f, 5.f,  (float)mConf.GetScreenSaverTime(), "m", false); break; }
    case MenuItemType::ScreensaverType: { AddList<RecalboxConf::Screensaver>(item, mProvider.GetTypeEntries(), mConf.GetScreenSaverType(), RecalboxConf::Screensaver::Dim, false); break; }
    case MenuItemType::ScreensaverSystems: { AddMultiList<String>(item, mProvider.GetSystemListAsString(), false); break; }
    case MenuItemType::PopupHelpDuration: { AddSlider(item, 0.f, 10.f, 1.f, 10.f, (float)mConf.GetPopupHelp(), "s", false); break; }
    case MenuItemType::PopupMusic: { AddSlider(item, 0.f, 10.f, 1.f, 5.f, (float)mConf.GetPopupMusic(), "s", false); break; }
    case MenuItemType::PopupNetplay: { AddSlider(item, 0.f, 10.f, 1.f, 8.f, (float)mConf.GetPopupNetplay(), "s", false); break; }
    case MenuItemType::ThemeSet: { AddList<ThemeSpec>(item, mProvider.GetThemeEntries(), mProvider.GetCurrentTheme(), mProvider.GetDefaultTheme(), false); break; }
    case MenuItemType::ThemeCarousel: { AddSwitch(item, mConf.GetThemeCarousel(), false); break; }
    case MenuItemType::ThemeTransition: { AddList<String>(item, mProvider.GetThemeTransitionEntries(), mConf.GetThemeTransition(), "slide", false); break; }
    case MenuItemType::ThemeRegion: { AddList<String>(item, mProvider.GetThemeRegionEntries(), mConf.GetThemeRegion(), "us", false); break; }
    case MenuItemType::ThemeOptColorset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("colorset")); break; }
    case MenuItemType::ThemeOptIconset: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("iconset")); break; }
    case MenuItemType::ThemeOptMenu: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("menu")); break; }
    case MenuItemType::ThemeOptSystemView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("systemview")); break; }
    case MenuItemType::ThemeOptGameView: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("gamelistview")); break; }
    case MenuItemType::ThemeOptGameClip: { String s = mConf.GetThemeFolder(); IniFile::PurgeKey(s); BuildThemeOptionSelector(item, mConf.GetThemeColorSet(s), ThemeManager::Instance().Main().GetSubSetValues("gameclipview")); break; }
    case MenuItemType::Brightness: { AddSlider(item, 0.f, 8.f, 1.f, 6.f, (float)mConf.GetBrightness(), "", false); break; }
    case MenuItemType::SystemSorting: { AddList<SystemSorting>(item, mProvider.GetSystemSortingEntries(), mConf.GetSystemSorting(), SystemSorting::Default, false); break; }
    case MenuItemType::QuickSelectSystem: { AddSwitch(item, mConf.GetQuickSystemSelect(), false); break; }
    case MenuItemType::OnScreenHelp: { AddSwitch(item, mConf.GetShowHelp(), false); break; }
    case MenuItemType::SwapValidateCancel: { AddSwitch(item, mConf.GetSwapValidateAndCancel(), false); break; }
    case MenuItemType::OSDClock: { AddSwitch(item, mConf.GetClock(), false); break; }
    case MenuItemType::DisplayByFilename: { AddSwitch(item, mConf.GetDisplayByFileName(), false); break; }
    case MenuItemType::UpdateGamelists: { AddAction(item, _("UPDATE!"), true, false); break; }
    case MenuItemType::HideSystems: { AddMultiList<SystemData*>(item, mProvider.GetHidableSystems(), false); break; }
    case MenuItemType::ArcadeEnhancedView: { AddSwitch(item, mConf.GetArcadeViewEnhanced(), false); break; }
    case MenuItemType::ArcadeFoldClones: { AddSwitch(item, mConf.GetArcadeViewFoldClones(), false); break; }
    case MenuItemType::ArcadeHideBios: { AddSwitch(item, mConf.GetArcadeViewHideBios(), false); break; }
    case MenuItemType::ArcadeHideNonWorking: { AddSwitch(item, mConf.GetArcadeViewHideNonWorking(), false); break; }
    case MenuItemType::ArcadeUseOfficielNames: { AddSwitch(item, mConf.GetArcadeUseDatabaseNames(), false); break; }
    case MenuItemType::ArcadeManufacturerSystems: { AddMultiList<String>(item, mProvider.GetManufacturersVirtualEntries(), false); break; }
    case MenuItemType::ArcadeAggregateEnable: { AddSwitch(item, mConf.GetCollectionArcade(), false); break; }
    case MenuItemType::ArcadeAggregateNeoGeo: { AddSwitch(item, mConf.GetCollectionArcadeNeogeo(), false); break; }
    case MenuItemType::ArcadeAggregateOriginal: { AddSwitch(item, mConf.GetCollectionArcadeHideOriginals(), false); break; }
    case MenuItemType::BootOnKodi: { AddSwitch(item, mConf.GetKodiAtStartup(), false); break; }
    case MenuItemType::BootDoNotScan: { AddSwitch(item, mConf.GetStartupGamelistOnly(), false); break; }
    case MenuItemType::AllowBootOnGame: { AddSwitch(item, mConf.GetAutorunEnabled(), false); break; }
    case MenuItemType::BootOnSystem: { AddList<String>(item, mProvider.GetBootSystemEntries(), mConf.GetStartupSelectedSystem(), SystemManager::sFavoriteSystemShortName, false); break; }
    case MenuItemType::BootOnGamelist: { AddSwitch(item, mConf.GetStartupStartOnGamelist(), false); break; }
    case MenuItemType::BootShowVideo: { AddSwitch(item, mConf.GetSplashEnabled(), false); break; }
    case MenuItemType::HideSystemView: { AddSwitch(item, mConf.GetStartupHideSystemView(), false); break; }
    case MenuItemType::VirtualAllGames: { AddSwitch(item, mConf.GetCollectionAllGames(), false); break; }
    case MenuItemType::VirtualMultiplayer: { AddSwitch(item, mConf.GetCollectionMultiplayer(), false); break; }
    case MenuItemType::VirtualLastPlayed: { AddSwitch(item, mConf.GetCollectionLastPlayed(), false); break; }
    case MenuItemType::VirtualLightgun: { AddSwitch(item, mConf.GetCollectionLightGun(), false); break; }
    case MenuItemType::VirtualPorts: { AddSwitch(item, mConf.GetCollectionPorts(), false); break;}
    case MenuItemType::VirtualPerGenre: { AddMultiList<GameGenres>(item, mProvider.GetGameGenreEntries(), false); break; }
    case MenuItemType::AdvDebugLogs: { AddSwitch(item, mConf.GetDebugLogs(), false); break; }
    case MenuItemType::AdvShowFPS: { AddSwitch(item, mConf.GetGlobalShowFPS(), false); break; }
    case MenuItemType::ResolutionGlobal: { AddList<String>(item, mProvider.GetGlobalResolutionEntries(), mConf.GetGlobalVideoMode(), ResolutionAdapter().DefaultResolution().ToString(), false); break; }
    case MenuItemType::ResolutionFrontEnd: { AddList<String>(item, mProvider.GetResolutionEntries(), mConf.GetEmulationstationVideoMode(), String::Empty, false); break; }
    case MenuItemType::ResolutionEmulators:
    {
      SelectorEntry<String>::List resolutions = mProvider.GetResolutionEntries();
      for(SystemData* system : mProvider.VisibleSystems())
        AddList<String>(item, resolutions, mConf.GetSystemVideoModeNoDefault(*system), String::Empty, false, true)->ReplaceParameters(system->FullName()).MergeContext(InheritableContext(system));
      break;
    }
    case MenuItemType::DeviceFreeSpace: { AddBar(item, (_F("{0} FREE") / Context().Device()->HumanFree())(), 1.0f - (float)((double)Context().Device()->Free / (double)Context().Device()->Size), false); break; }
    case MenuItemType::DeviceName: { AddText(item, Context().Device()->DisplayableShortName())->Parent().SetTitle(Context().Device()->DisplayableShortName()); break; }
    case MenuItemType::DeviceFS: { AddText(item, Context().Device()->FileSystem); break; }
    case MenuItemType::DeviceNetworkAccess: { AddText(item, Context().Device()->MountPoint.empty() ? _("NO ACCESS") : String("\\\\RECALBOX\\share\\externals\\").Append(Path(Context().Device()->MountPoint).Filename())); break; }
    case MenuItemType::DeviceUsable: { AddText(item, Context().Device()->IsNTFS() ? _("YES, BUT NOT RECOMMENDED") : Context().Device()->RecalboxCompatible() ? _("YES") : _("NO")); break; }
    case MenuItemType::DeviceInitialize:
    {
      bool alreadyInitialized = mProvider.SystemManager().HasRomStructure(Path(Context().Device()->MountPoint));
      AddAction(item, _("INITIALIZE!"), true, !(Context().Device()->RecalboxCompatible() && !alreadyInitialized)); break;
    }
    case MenuItemType::WebManagerEnable: { AddSwitch(item, mConf.GetSystemManagerEnabled(), false); break; }
    case MenuItemType::FactoryReset: { AddAction(item, _("RESET!"), true, false); break; }
    case MenuItemType::Overclock: { AddList<Overclocking::Overclock>(item, mProvider.GetOverclockEntries(), mProvider.Overclocking().GetCurrentOverclock(), mProvider.Overclocking().NoOverclock(), false); break; }
    case MenuItemType::PerSystemEmulator: { String emulatorAndCore; String defaultEmulatorAndCore; AddList(item, mProvider.GetEmulatorEntries(Context().System(), emulatorAndCore, defaultEmulatorAndCore), emulatorAndCore, defaultEmulatorAndCore, false); break; }
    case MenuItemType::PerSystemRatio: { AddList(item, mProvider.GetRatioEntries(), mConf.GetSystemRatio(*Context().System()), String::Empty, false); break; }
    case MenuItemType::PerSystemSmooth: { AddSwitch(item, mConf.GetSystemSmooth(*Context().System()),false); break; }
    case MenuItemType::PerSystemRewind: { AddSwitch(item, mConf.GetSystemRewind(*Context().System()), false); break; }
    case MenuItemType::PerSystemAutoLoadSave: { AddSwitch(item, mConf.GetSystemAutoSave(*Context().System()), false); break; }
    case MenuItemType::PerSystemShaderSet: { AddList(item, mProvider.GetShadersEntries(), mConf.GetSystemShaders(*Context().System()), String::Empty, false); break; }
    case MenuItemType::PerSystemShaders: { AddList(item, mProvider.GetShaderPresetsEntries(), mConf.GetSystemShaderSet(*Context().System()), String("none"), false); break; }
    case MenuItemType::BootloaderUpdate: { AddAction(item, _("UPDATE!"), true, false); break; }
    case MenuItemType::CrtAdapter: { AddList<CrtAdapterType>(item, mProvider.GetDacEntries(), mResolver.HasRRGBD() ? CrtAdapterType::RGBDual : CrtConf::Instance().GetSystemCRT(), CrtAdapterType::None, false); break; }
    case MenuItemType::CrtMenuResolution: { AddList<String>(item, mProvider.GetEsResolutionEntries(), mCrtConf.GetSystemCRTResolution(), "0", false); break; }
    case MenuItemType::CrtScreenType:
    {
      // Polymorphic item
      if (mResolver.HasJamma()) AddList<ICrtInterface::HorizontalFrequency>(item, mProvider.GetHorizontalOutputFrequency(), Board::Instance().CrtBoard().GetHorizontalFrequency(), ICrtInterface::HorizontalFrequency::KHz15, false);
      else AddText(item, ICrtInterface::HorizontalFrequencyToHumanReadable(Board::Instance().CrtBoard().GetHorizontalFrequency()));
      break;
    }
    case MenuItemType::CrtForce50hz:
    {
      String result(Board::Instance().CrtBoard().MustForce50Hz() ? _("ON") : _("OFF")); result.Append(' ').Append(_("(Hardware managed)"));
      if (Board::Instance().CrtBoard().HasForced50hzSupport()) AddText(item, result);
      break;
    }
    case MenuItemType::CrtHDMIPriority: { AddSwitch(item, mCrtConf.GetSystemCRTForceHDMI(), false); break; }
    case MenuItemType::CrtSelectRefreshAtLaunch: { AddSwitch(item, mCrtConf.GetSystemCRTGameRegionSelect(), false); break; }
    case MenuItemType::CrtSelectResolutionAtLaunch: { AddSwitch(item, mCrtConf.GetSystemCRTGameResolutionSelect(), false); break; }
    case MenuItemType::CrtSuperrezMultiplier: { AddList<String>(item, mProvider.GetSuperRezEntries(), mCrtConf.GetSystemCRTSuperrez(), "1920",  false); break; }
    case MenuItemType::CrtForceJack: { AddSwitch(item, mCrtConf.GetSystemCRTForceJack(), false); break; }
    case MenuItemType::CrtScanline240in480: { AddList<CrtScanlines>(item, mProvider.GetScanlinesEntries(), mCrtConf.GetSystemCRTScanlines31kHz(), CrtScanlines::None, Board::Instance().CrtBoard().GetHorizontalFrequency() < ICrtInterface::HorizontalFrequency::KHz31); break;}
    case MenuItemType::CrtDemo240: { AddSwitch(item, mCrtConf.GetSystemCRTRunDemoIn240pOn31kHz(), Board::Instance().CrtBoard().GetHorizontalFrequency() < ICrtInterface::HorizontalFrequency::KHz31); break; }
    case MenuItemType::JammaSound: { AddList<String>(item, { { "JACK/MONO", "0" }, { "JACK/PINS", "1" } }, CrtConf::Instance().GetSystemCRTJammaAmpDisable() ? "1" : "0", "0", false);break; }
    case MenuItemType::JammaAmpBoost: { AddList<String>(item, { { "default", "0" }, { "+6dB", "1" }, { "+12dB", "2" }, { "+16dB", "3" } }, CrtConf::Instance().GetSystemCRTJammaMonoAmpBoost(), "0", false); break;}
    case MenuItemType::JammaPanelType: { AddList<String>(item, { { "2 buttons", "2" }, { "3 buttons", "3" }, { "4 buttons", "4" }, { "5 buttons", "5" }, { "6 buttons", "6" } }, CrtConf::Instance().GetSystemCRTJammaPanelButtons(), "2", false); break; }
    case MenuItemType::JammaNeogeoLayoutP1: { AddList<String>(item, { { "Default", "neodefault" }, { "Line", "line" }, { "Square", "square" } }, CrtConf::Instance().GetSystemCRTJammaNeogeoLayoutP1(), "neodefault", false); break; }
    case MenuItemType::JammaNeogeoLayoutP2: { AddList<String>(item, { { "Default", "neodefault" }, { "Line", "line" }, { "Square", "square" } }, CrtConf::Instance().GetSystemCRTJammaNeogeoLayoutP2(), "neodefault", false); break; }
    case MenuItemType::Jamma4PlayerMode: { AddSwitch(item, mCrtConf.GetSystemCRTJamma4Players(), false); break; }
    case MenuItemType::JammaCredit: { AddSwitch(item, mCrtConf.GetSystemCRTJammaStartBtn1Credit(), false); break; }
    case MenuItemType::JammaHotkey: { AddSwitch(item, mCrtConf.GetSystemCRTJammaHKOnStart(), false); break; }
    case MenuItemType::JammaVolume: { AddSwitch(item, mCrtConf.GetSystemCRTJammaSoundOnStart(), false); break; }
    case MenuItemType::JammaExit: { AddSwitch(item, mCrtConf.GetSystemCRTJammaExitOnStart(), false); break; }
    case MenuItemType::JammaAutoFire: { AddSwitch(item, mCrtConf.GetSystemCRTJammaAutoFire(), false); break; }
    case MenuItemType::JammaDualJoystick: { AddSwitch(item, mCrtConf.GetSystemCRTJammaDualJoysticks(), false); break; }
    case MenuItemType::JammaPinE27: { AddSwitch(item, mCrtConf.GetSystemCRTJammaButtonsOnJamma() != "6", false); break; }
    case MenuItemType::JammaReset: { AddAction(item, _("RESET"), true, false); break; }
    case MenuItemType::CrtReducedLantency: { AddSwitch(item, mConf.GetGlobalReduceLatency(), false); break; }
    case MenuItemType::CrtRunAhead: { AddSwitch(item, mConf.GetGlobalRunAhead(), false); break; }
    case MenuItemType::CrtScreenCalibration: { AddAction(item, _("CALIBRATE"), true, false); break; }
    case MenuItemType::_Error_:
    default: { LOG(LogFatal) << "[MenuProvider] Cannot create menu item: " << (int)item.Type(); break; }
  }
}

/*
 * Following method store & check menu items
 * Most of the menu items need to be singletons and are so checked against duplication
 * However some contextualized items **are** duplicated and reconized only by their context
 * Such items do not need to be checked and are not stored in type => item map so that they cannot be retrieved
 * later using their identifier.
 */

void MenuBuilder::CheckDuplicate(const ItemDefinition& item, bool acceptDuplicates)
{
  if (!acceptDuplicates)
   if (mItemsTypes.contains(item.Type()))
      { LOG(LogFatal) << "[MenuBuildert] Item of type " << (int)item.Type() << " captionned '" << item.RawCaption() << "' is duplicated in current menu!"; }
}

ItemText* MenuBuilder::AddText(const ItemDefinition& item, const String& text, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemText* object = Menu::AddText(item.Caption(this), text, item.Help());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemBar* MenuBuilder::AddBar(const ItemDefinition& item, const String& text, float ratio, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemBar* object = Menu::AddBar(item.Caption(this), text, ratio, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemAction* MenuBuilder::AddAction(const ItemDefinition& item, const String& action, bool positive, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemAction* object = Menu::AddAction(item.Icon(), item.Caption(this), _S(action), (int)item.Type(), positive, &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSwitch* MenuBuilder::AddSwitch(const ItemDefinition& item, bool state, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSwitch* object = Menu::AddSwitch(item.Caption(this), state, (int)item.Type(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemEditable* MenuBuilder::AddEditor(const ItemDefinition& item, const String& editTitle, const String& text, bool masked, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemEditable* object = AddEditable(editTitle, item.Caption(this), text, (int)item.Type(), &mProvider, item.Help(), masked, grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemEditable* MenuBuilder::AddEditor(const ItemDefinition& item, const String& text, bool masked, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemEditable* object = AddEditable(item.Caption(this), text, (int)item.Type(), &mProvider, item.Help(), masked, grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSlider*
MenuBuilder::AddSlider(const ItemDefinition& item, float min, float max, float inc, float value, float defaultvalue,
                       const String& suffix, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSlider* object = Menu::AddSlider(item.Caption(this), min, max, inc, value, defaultvalue, suffix, (int)item.Type(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

ItemSubMenu* MenuBuilder::AddSubMenu(const ItemDefinition& item, bool grayed)
{
  // Menu item are duplicable by default, and they are not stored.
  ItemSubMenu* object = Menu::AddSubMenu(item.Caption(this), (int)item.MenuType(), &mProvider, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  return object;
}

template<class T>
ItemSelector<T>* MenuBuilder::AddList(const ItemDefinition& item, const SelectorEntry<T>::List& values, const T& value, const T& defaultValue, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSelector<T>* object = Menu::AddList<T>(item.Caption(this), (int)item.Type(), &mProvider, values, value, defaultValue, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

template<class T>
ItemSelector<T>* MenuBuilder::AddMultiList(const ItemDefinition& item, const SelectorEntry<T>::List& values, bool grayed, bool acceptDuplicates)
{
  CheckDuplicate(item, acceptDuplicates);
  ItemSelector<T>* object = Menu::AddMultiList<T>(item.Caption(this), (int)item.Type(), &mProvider, values, item.Help(), grayed, item.UnselectableHelp());
  object->MergeContext(Context());
  mItemsLinks[object] = &item;
  if (!acceptDuplicates) mItemsTypes[item.Type()] = object;
  return object;
}

