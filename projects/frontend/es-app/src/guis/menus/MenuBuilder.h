//
// Created by bkg2k on 21/10/24.
//
#pragma once

#include <guis/menus/MenuDefinition.h>
#include "guis/menus/base/Menu.h"
#include "utils/eval/SimpleExpressionEvaluator.h"
#include "guis/menus/tasks/IMenuBackgroundTask.h"
#include "CrtConf.h"

// Forward declaration
class WindowManager;
class MenuProvider;
class MenuProcessor;

class MenuBuilder : public Menu
                  , public SimpleExpressionEvaluator::IIdentifierEvaluator
{
  public:
    /*!
     * @brief Constructor
     * @param definition Menu definition
     * @param menuProcessor Menu event processor
     */
    MenuBuilder(WindowManager& window, const InheritableContext& context, MenuProvider& provider, const MenuDefinition& definition, IGlobalVariableResolver& resolver, const ItemBase* parentItem)
      : Menu(window, context, String('*').Append((parentItem != nullptr ? parentItem->Label() : definition.mCaption)), String::Empty, definition.mAnimated)
      , mCrtConf(CrtConf::Instance())
      , mProvider(provider)
      , mResolver(resolver)
      , mDefinition(definition)
    {}

    //! Destructor
    ~MenuBuilder();

    /*!
     * @brief Inherited classes must add items in here
     */
    void BuildMenuItems() final;

    /*
     * Getter
     */

    ItemHeader* AsHeader(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsHeader(); return nullptr; }
    ItemSubMenu* AsSubMenu(MenuItemType type)                        const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsSubMenu(); return nullptr; }
    ItemText* AsInfoText(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsText(); return nullptr; }
    ItemSwitch* AsSwitch(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsSwitch(); return nullptr; }
    ItemSlider* AsSlider(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsSlider(); return nullptr; }
    template<typename T> ItemSelector<T>* AsList(MenuItemType type)  const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsList<T>(); return nullptr; }
    template<typename T> ItemSelector<T>* AsMulti(MenuItemType type) const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsMulti<T>(); return nullptr; }
    ItemEditable* AsEditor(MenuItemType type)                        const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsEditor(); return nullptr; }
    ItemRating* AsRating(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsRating(); return nullptr; }
    ItemAction* AsAction(MenuItemType type)                          const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsAction(); return nullptr; }
    ItemBar* AsBar(MenuItemType type)                                const { if (ItemBase** base = mItemsTypes.try_get(type); base != nullptr) return (*base)->AsBar(); return nullptr; }

    // Allow public call
    using Menu::RequestReboot;
    using Menu::RequestRelaunch;

  private:
    //! Crt Configuration
    CrtConf& mCrtConf;
    //! Provider
    MenuProvider& mProvider;
    //! Global resolver
    IGlobalVariableResolver& mResolver;
    //!< Definition
    const MenuDefinition mDefinition;

    //! Background tasks
    Array<IMenuBackgroundTask*> mTasks;

    //! Menu map: menu item to item definition
    HashMap<ItemBase*, const ItemDefinition*> mItemsLinks;
    //! Menu map: menu type to menu item
    HashMap<MenuItemType, ItemBase*> mItemsTypes;

    //! Check if an item is duplicated in the current menu. If yes, log & abort
    void CheckDuplicate(const ItemDefinition& item, bool acceptDuplicates);

    //! Add background task
    void AddTask(IMenuBackgroundTask* task) { mTasks.Add(task); }

    /*
     * Component
     */

    void onShow() final
    {

    }

    /*
     * Reboot/Relaunch management
     */

    /*!
     * @brief Called when the user delays the reboot action
     * @param window Window manager instance
     */
    static void RebootPending(WindowManager* window);

    /*
     * Item creation
     */

    //! Add given item according to its type
    void AddItem(const ItemDefinition& item);

    /*!
     * @brief Add custom submenu(s)
     * @param subMenuItem SubMenu item
     * @return True if one or more submenu item have been added, false otherwise
     */
    bool AddCustomSubMenu(const ItemDefinition& subMenuItem);

    /*!
     * @brief Called on every item when the menu is shown
     * @param item Item base
     */
    void MenuItemShow(ItemBase& item);

    //! Add submenu
    ItemSubMenu* AddSubMenu(const ItemDefinition& item, bool grayed);
    //! Text item helper
    ItemText* AddText(const ItemDefinition& item, const String& text, bool acceptDuplicates = false);
    //! Bar item helper
    ItemBar* AddBar(const ItemDefinition& item, const String& text, float ratio, bool grayed, bool acceptDuplicates= false);
    //! Switch item helper
    ItemSwitch* AddSwitch(const ItemDefinition& item, bool state, bool grayed, bool acceptDuplicates = false);
    //! Action item helper
    ItemAction* AddAction(const ItemDefinition& item, const String& action, bool positive, bool grayed, bool acceptDuplicates = false);
    //! Editor item helper
    ItemEditable* AddEditor(const ItemDefinition& item, const String& editTitle, const String& text, bool masked, bool grayed, bool acceptDuplicates = false);
    ItemEditable* AddEditor(const ItemDefinition& item, const String& text, bool masked, bool grayed, bool acceptDuplicates = false);
    //! Slider helper
    ItemSlider* AddSlider(const ItemDefinition& item, float min, float max, float inc, float defaultvalue, float value, const String& suffix, bool grayed, bool acceptDuplicates = false);
    //! List item helper
    template<class T> ItemSelector<T>* AddList(const ItemDefinition& item, const SelectorEntry<T>::List& values, const T& value, const T& defaultValue, bool grayed, bool acceptDuplicates = false);
    //! Multi List item helper
    template<class T> ItemSelector<T>* AddMultiList(const ItemDefinition& item, const SelectorEntry<T>::List& values, bool grayed, bool acceptDuplicates = false);

    /*
     * Special item creation / Refresh
     */

    /*!
     * @brief Build an Item selector for a particular theme option set
     * @param item Menu item definition
     * @param selected Selected value
     * @param items Option set values
     */
    void BuildThemeOptionSelector(const ItemDefinition& item, const String& selected, const String::List& items);

    /*!
     * @brief Try to sort item list by their front numeric value when all items have numeric values
     * If at least one item has no numeric value, the list is sorted alphanumerically
     * @param list String list to sort
     * @return Sorted list
     */
    bool TrySortNumerically(SelectorEntry<String>::List& list);

    /*
     * SimpleExpressionEvaluator::IIdentifierEvaluator
     */

    /*!
     * @brief Must return a boolean evaluation of the given identifier
     * @param identifier String identifier to evaluate
     * @return True or false depending of the identifier evaluation
     */
    bool EvaluateIdentifier(const String& identifier) final;
};


