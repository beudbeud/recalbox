//
// Created by bkg2k on 21/11/24.
//
#pragma once

#include "hardware/devices/storage/StorageDevices.h"

//! Forward declarations
class SystemData;
class FileData;

class InheritableContext
{
  public:
    //! Always merge, never replace
    InheritableContext(const InheritableContext& source)
      : mSystemData(nullptr)
      , mGameData(nullptr)
      , mDevice(nullptr)
    {
      Merge(source);
    }
    //! Delete move constructor
    InheritableContext(InheritableContext&& source) = delete;

    //! Default constructor
    InheritableContext()
      : mSystemData(nullptr)
      , mGameData(nullptr)
      , mDevice(nullptr)
    {}

    //! System context
    explicit InheritableContext(SystemData* system)
      : mSystemData(system)
      , mGameData(nullptr)
      , mDevice(nullptr)
    {}

    //! System/game context
    InheritableContext(SystemData* system, FileData* game)
      : mSystemData(system)
      , mGameData(game)
      , mDevice(nullptr)
    {}

    //! Device context
    explicit InheritableContext(const StorageDevices::Device* device)
      : mSystemData(nullptr)
      , mGameData(nullptr)
      , mDevice(device)
    {}

    /*!
     * @brief Merge source data. Definied source data overwrite current data
     * @param source Source to merge
     * @return This
     */
    InheritableContext& Merge(const InheritableContext& source)
    {
      // Copy non-null source data only - Keep al others
      if (source.mSystemData != nullptr) mSystemData = source.mSystemData;
      if (source.mGameData != nullptr) mGameData = source.mGameData;
      if (source.mDevice != nullptr) mDevice = source.mDevice;
      return *this;
    }

    /*
     * Getters
     */

    //! Has system?
    [[nodiscard]] bool HasSystem() const { return mSystemData != nullptr; }
    //! Has game
    [[nodiscard]] bool HasGame() const { return mGameData != nullptr; }
    //! Has device
    [[nodiscard]] bool HasDevice() const { return mDevice != nullptr; }

    //! Get system
    [[nodiscard]] SystemData* System() const { return mSystemData; }
    //! Get game
    [[nodiscard]] FileData* Game() const { return mGameData; }
    //! Get dvice
    [[nodiscard]] const StorageDevices::Device* Device() const { return mDevice; }

  private:
    //! System
    SystemData* mSystemData;
    //! Game
    FileData* mGameData;
    //! Storage device
    const StorageDevices::Device* mDevice;
};
