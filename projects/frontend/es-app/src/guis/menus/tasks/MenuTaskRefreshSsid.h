//
// Created by bkg2k on 10/11/24.
//
#pragma once

#include "utils/os/system/Thread.h"
#include "IMenuBackgroundTask.h"
#include "guis/menus/base/ItemSelector.h"

class MenuTaskRefreshSSID : public IMenuBackgroundTask
                          , private ISyncMessageReceiver<void>
{
  public:
    MenuTaskRefreshSSID(ItemSelector<String>& ssids)
      : IMenuBackgroundTask("RefreshSSID")
      , mSender(*this)
      , mSSIDs(ssids)
    {
    }

  private:
    //! Sender
    SyncMessageSender<void> mSender;
    //! SSID list item
    ItemSelector<String>& mSSIDs;
    //! Last list
    String::List mLastList;
    //! Last list guardian
    Mutex mListGuard;

    /*
     * IMenuBackgroundTask
     */

    void TaskRun() final;

    /*
     * Synchronous event
     */

    /*!
     * @brief Receive message from the command thread
     * @param game Game data
     */
    void ReceiveSyncMessage() final;
};
