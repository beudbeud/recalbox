//
// Created by bkg2k on 10/11/24.
//

#include "MenuTaskRefreshSsid.h"
#include "recalbox/RecalboxSystem.h"

void MenuTaskRefreshSSID::TaskRun()
{
  while(Running())
  {
    DateTime start;
    String::List list = RecalboxSystem::getAvailableWiFiSSID(RecalboxConf::Instance().GetWifiEnabled());
    if (list.size() == 1 && list[0].empty()) list.clear(); // remove empty items
    { Mutex::AutoLock lock(mListGuard); mLastList = std::move(list); }
    mSender.Send();
    DateTime end;
    if (TimeSpan span = end - start; span.TotalSeconds() < 10)
      for(int i = (10000 - span.TotalMilliseconds()) / 20; --i >= 0; )
      {
        if (!Running()) break;
        Thread::Sleep(20);
      }
  }
}

void MenuTaskRefreshSSID::ReceiveSyncMessage()
{
  // Rebuild list
  ItemSelector<String>::List newList;
  {
    Mutex::AutoLock lock(mListGuard);
    for(const String& ssid : mLastList)
      newList.push_back({ ssid, ssid });
  }
  if (newList.empty()) newList.push_back({ _("NO WIFI ACCESS POINT"), String::Empty });

  // Refresh item
  String previousSelectedValue = mSSIDs.SelectedValue();
  mSSIDs.ChangeSelectionItems(newList, previousSelectedValue, String::Empty);
}
