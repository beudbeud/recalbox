//
// Created by bkg2k on 17/10/24.
//
#pragma once

#include "MenuItemType.h"
#include "guis/menus/base/Menu.h"
#include "utils/eval/SimpleExpressionEvaluator.h"
#include "MenuDefinition.h"
#include "ResolutionAdapter.h"
#include "guis/GuiWaitLongExecution.h"
#include "guis/menus/vault/MenuThemeOptions.h"
#include <systems/SystemManager.h>
#include <hardware/devices/storage/StorageDevices.h>
#include <scraping/scrapers/ScrapingMethod.h>
#include <guis/menus/MenuBuilder.h>
#include <Upgrade.h>
#include <hardware/Overclocking.h>

class MenuProvider : public StaticLifeCycleControler<MenuProvider>
                   // Sync/delayed message
                   , public ISyncMessageReceiver<const ItemSelectorBase<ThemeSpec>*>
                   // Item callbacks
                   , public IActionTriggered
                   , public ISwitchChanged
                   , public IEditableChanged
                   , public ISubMenuSelected
                   , public ISliderChanged
                   // Selector callbacks
                   , public ISingleSelectorChanged<String>
                   , public ISingleSelectorChanged<StorageDevices::Device>
                   , public ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperImageType>
                   , public ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperVideoType>
                   , public ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperRegionPriority>
                   , public ISingleSelectorChanged<Languages>
                   , public ISingleSelectorChanged<Regions::GameRegions>
                   , public ISingleSelectorChanged<ScraperNameOptions>
                   , public ISingleSelectorChanged<ScraperType>
                   , public ISingleSelectorChanged<ScrapingMethod>
                   , public ISingleSelectorChanged<RecalboxConf::SoftPatching>
                   , public ISingleSelectorChanged<RecalboxConf::Relay>
                   , public ISingleSelectorChanged<RecalboxConf::UpdateType>
                   , public ISingleSelectorChanged<RecalboxConf::Screensaver>
                   , public ISingleSelectorChanged<ThemeSpec>
                   , public ISingleSelectorChanged<SystemSorting>
                   , public ISingleSelectorChanged<Overclocking::Overclock>
                   , public ISingleSelectorChanged<CrtAdapterType>
                   , public ISingleSelectorChanged<ICrtInterface::HorizontalFrequency>
                   , public ISingleSelectorChanged<CrtScanlines>
                   , public IMultiSelectorChanged<SystemData*>
                   , public IMultiSelectorChanged<String>
                   , public IMultiSelectorChanged<GameGenres>
{
  public:
    // Recalbox theme compatibility
    static constexpr int sRecalboxMinimumCompatibilityVersion = 0x902; // (Major << 8) + Minor

    //! Theme spec list
    typedef std::vector<ThemeSpec> ThemeSpecList;

    //! Persistant scraper data accross scraping sessions
    struct ScraperPersistentData
    {
      Array<SystemData*> mSystems; //!< Selected systems
      ScrapingMethod mMethod;      //!< Scraping method
      ScraperPersistentData(ScrapingMethod method) : mSystems(256), mMethod(method) {}
    };

    //! Constructor
    MenuProvider(WindowManager& window, SystemManager& systemManager, IGlobalVariableResolver& resolver)
      : StaticLifeCycleControler<MenuProvider>("menuprovider")
      , mWindow(window)
      , mSystemManager(systemManager)
      , mResolver(resolver)
      , mConf(RecalboxConf::Instance())
      , mCrtConf(CrtConf::Instance())
      , mSender(*this)
      , mScraperData(ScrapingMethod::AllIfNothingExists)
    {
      DeserializeMenus();
    }

    /*!
     * @brief Build and show the given menu. Log & abort() if the menu does not exist
     * @param identifier Menu name, as defined in the xml file
     */
    static void ShowMenu(MenuContainerType identifier, const InheritableContext& context)
    {
      MenuProvider& This = Instance();
      This.BuildMenu(identifier, context, nullptr);
    }

    MenuDefinition* GetMenuDefinition(MenuContainerType menuType)
    {
      MenuDefinition* menu = mMenuDefinitions.try_get(menuType);
      if (menu == nullptr) { LOG(LogError) << "[MenuProvider] Unknown submenu identifier '" << (int)menuType; return nullptr; }
      return menu;
    }

    /*
     * Accessors
     */

    //! Get current system
    [[nodiscard]] ::SystemManager& SystemManager() const { return mSystemManager; }

    //! Scraper data
    const ScraperPersistentData ScraperData() const { return mScraperData; }

    //! Overclocking
    ::Overclocking& Overclocking() { return mOverclocking; }

    /*
     * Menu helpers
     */

    /*!
     * @brief Check if a menu is selectable or must be grayed. All submenu are selectable by defayt
     * @param identifier Submenu identifier
     * @return True to make the menu selectable, false otherwose
     */
    bool IsMenuUnselectable(MenuContainerType identifier);

    //! Get storage devices
    [[nodiscard]] const ::StorageDevices& StorageDevices() const { return mStorageDevices; }

    /*
     * Menu fillers
     */

    //! Get version string
    static String GetVersionString() { return Upgrade::Instance().CurrentVersion().Append(" (").Append(Board::Instance().GetBoardName()).Append(' ').Append(Board::Instance().GetDataBusSize()).Append("bits").Append(')'); }
    //! Get Storage List
    SelectorEntry<StorageDevices::Device>::List GetStorageEntries(int& currentIndex);
    //! Get Culture List
    static SelectorEntry<String>::List GetCultureEntries();
    //! Get Keyboard List
    static SelectorEntry<String>::List GetKeyboardEntries();
    //! Get TimeZone List
    static SelectorEntry<String>::List GetTimeZones();
    //! Get resolution list for Kodi
    SelectorEntry<String>::List GetKodiResolutionsEntries();
    //! Get available scraping methods
    static SelectorEntry<ScrapingMethod>::List GetScrapingMethods();
    //! Get scrapable system list
    SelectorEntry<SystemData*>::List GetScrapableSystemsEntries();
    //! Get available scrappers
    static SelectorEntry<ScraperType>::List GetScrapersEntries();
    //! Get name retrieval methods when scraping
    static SelectorEntry<ScraperNameOptions>::List GetScraperNameOptionsEntries();
    //! Get available scraped image types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List GetScraperImagesEntries();
    //! Get available scraped thumbnail types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperImageType>::List GetScraperThumbnailsEntries();
    //! Get available scraped video types
    static SelectorEntry<ScreenScraperEnums::ScreenScraperVideoType>::List GetScraperVideosEntries();
    //! Get scraped region priority
    static SelectorEntry<ScreenScraperEnums::ScreenScraperRegionPriority>::List GetScraperRegionOptionsEntries();
    //! Get favorite region when scrping
    static SelectorEntry<Regions::GameRegions>::List GetScraperRegionsEntries();
    //! Get available scraped languages
    static SelectorEntry<Languages>::List GetScraperLanguagesEntries();
    //! Get scraper login
    String GetScraperLogin(ScraperType type);
    //! Get scraper password
    String GetScraperPassword(ScraperType type);
    //! Set scraper login
    void SetScraperLogin(ScraperType type, const String& login);
    //! Set scraper password
    void SetScraperPassword(ScraperType type, const String& password);
    //! Credential required for the current scraper?
    bool HasScraperCredentials(ScraperType type);
    //! Get Softpatching option list
    static SelectorEntry<RecalboxConf::SoftPatching>::List& GetSoftpatchingEntries();
    //! Get Bameboy mod entries
    SelectorEntry<String>::List& GetSuperGameBoyEntries();
    //! Get libretro ratios
    SelectorEntry<String>::List GetRatioEntries();
    //! Get Shaders List
    static SelectorEntry<String>::List GetShadersEntries();
    //! Get shader sets List
    static SelectorEntry<String>::List& GetShaderPresetsEntries();
    //! Get Netplay relay server list
    SelectorEntry<RecalboxConf::Relay>::List& GetMitmEntries();
    //! Get Update type List
    static SelectorEntry<RecalboxConf::UpdateType>::List GetUpdateTypeEntries();
    //! Get screensavers
    SelectorEntry<RecalboxConf::Screensaver>::List GetTypeEntries();
    //! Get all system list as string
    SelectorEntry<String>::List GetSystemListAsString();
    //! Get all available themes
    SelectorEntry<ThemeSpec>::List GetThemeEntries();
    //! Get current theme
    ThemeSpec GetCurrentTheme();
    //! Get default theme
    ThemeSpec GetDefaultTheme();
    // Get theme transitions
    SelectorEntry<String>::List GetThemeTransitionEntries();
    // Get theme regions
    SelectorEntry<String>::List GetThemeRegionEntries();
    //! Get theme display list
    String GetDisplayList(ThemeData::Compatibility display);
    //! Get theme resolution group list
    String GetResolutionList(ThemeData::Resolutions resolutions);
    //! Get System sorting list
    static SelectorEntry<SystemSorting>::List& GetSystemSortingEntries();
    //! Get hidable system list
    SelectorEntry<SystemData*>::List GetHidableSystems();
    //! Get manufacturer list
    SelectorEntry<String>::List GetManufacturersVirtualEntries();
    //! Boot system list
    SelectorEntry<String>::List GetBootSystemEntries();
    //! Game genre list
    SelectorEntry<GameGenres>::List GetGameGenreEntries();
    //! Get global resolution list
    SelectorEntry<String>::List GetGlobalResolutionEntries();
    //! Get frontend resolution list
    SelectorEntry<String>::List GetResolutionEntries();
    //! Get overclock list
    SelectorEntry<Overclocking::Overclock>::List GetOverclockEntries();
    //! Get emulator list for the given system
    SelectorEntry<String>::List GetEmulatorEntries(const SystemData* system, String& emulatorAndCore, String& defaultEmulatorAndCore);
    //! Get RGB Adapter
    SelectorEntry<CrtAdapterType>::List GetDacEntries();
    // Get Crt Frontend resolution
    SelectorEntry<String>::List GetEsResolutionEntries();
    //! Crt horizontal frequencies
    static const SelectorEntry<ICrtInterface::HorizontalFrequency>::List& GetHorizontalOutputFrequency();
    //! Get CRT's super resolution list
    static const SelectorEntry<String>::List& GetSuperRezEntries();
    //! Get CRT's scanline list
    static const SelectorEntry<CrtScanlines>::List& GetScanlinesEntries();

    //! Format system name with emulator/core
    String SystemNameWithEmulator(const SystemData* system);

    /*
     * Menu filler helpers
     */

    //! Get visible system list
    const SystemManager::List& VisibleSystems() const { return mSystemManager.VisibleSystemList(); }

    //! Get all systems, visible or not
    const SystemManager::List& AllSystems() const { return mSystemManager.AllSystems(); }

  private:
    //! Pointer to RecalboxConf::SetBool methods
    typedef RecalboxConf& (RecalboxConf::*SetBoolMethod)(const bool&);

    //! Window manager reference
    WindowManager& mWindow;
    //! SystemManager reference
    ::SystemManager& mSystemManager;
    //! Variable resolver reference
    IGlobalVariableResolver& mResolver;
    //! Reclabox configuration reference
    RecalboxConf& mConf;
    //! Crt configuration reference
    CrtConf& mCrtConf;

    //! Menus definititons id:definition
    HashMap<MenuContainerType, MenuDefinition> mMenuDefinitions;

    // Message
    SyncMessageSender<const ItemSelectorBase<ThemeSpec>*> mSender;

    /*
     * Object required by menus
     */

    //! Storage devices
    ::StorageDevices mStorageDevices;
    //! Resolution Adapter
    ResolutionAdapter mResolutionAdapter;

    //! Persistant data accross scraping sessions
    ScraperPersistentData mScraperData;
    //! Last selected theme
    ThemeSpec mLastSelectedTheme;

    // Overclocking
    ::Overclocking mOverclocking;

    /*!
     * @brief Deserialize menu.xml
     */
    void DeserializeMenus();

    /*!
     * @brief Deserialize single menu
     * @param menu Menu xml node
     * @param items Item list in which to add new deserialized submenu - null for top level menus menu
     */
    void DeserializeMenu(const String& parentId, const XmlNode& menu, std::vector<ItemDefinition>* items);

    /*!
     * @brief Deserialize menu reference
     * @param parentId parent menu identifier
     * @param items Item list in which to add new deserialized reference
     * @param menuRef menu reference node
     */
    void DeserializeMenuRef(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& menuRef);

    /*!
     * @brief Deserialize single item
     * @param parentId parent menu identifier
     * @param items Item list in which to add new deserialized item
     * @param item Item xml node
     */
    void DeserializeItem(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& item);

    /*!
     * @brief Deserialize header
     * @param parentId parent menu identifier
     * @param items Item list in which to add new deserialized header
     * @param header Header xml node
     */
    void DeserializeHeader(const String& parentId, std::vector<ItemDefinition>& items, const XmlNode& header);

    /*!
     * @brief Check all submenu reference and verify reference ID are existing menus
     */
    void CheckMenuReferences();

    /*!
     * @brief Build & display a menu from a string identifier
     * @param identifier Menu identifier
     * @param context Context to merge
     * @param itemParent Item that created this menu - may be null
     */
    void BuildMenu(MenuContainerType identifier, const InheritableContext& context, const ItemBase* itemParent);

    /*!
     * @brief Build & display a menu from a menu definition
     * @param definition Menu definition
     * @param context Context to merge
     * @param itemParent Item that created this menu - may be null
     */
    void BuildMenu(const MenuDefinition& definition, const InheritableContext& context, const ItemBase* itemParent);

    /*!
     * @brief Check an item against allowed properties. Fail if at least one attribute is unknown
     * @param parentId Parent menu id
     * @param allowedProperties List of allowed properties
     * @param node XML node to check
     */
    void CheckProperties(const String& parentId, const Array<const char*>& allowedProperties, const XmlNode& node);

    /*
     * ISyncReceiveMessage<void> implementation
     */

    /*!
     * @brief Update theme options
     */
    void ReceiveSyncMessage(const ItemSelectorBase<ThemeSpec>* const& sourceItem) final;

    /*
     * Co-Items helpers
     */

    //! Get the parent builder of the current itam
    MenuBuilder& Builder(const ItemBase& item) { return ((MenuBuilder&)item.Parent()); }

    ItemSwitch& CoItemAsSwitch(const ItemBase& fromCoItem, MenuItemType itemType)
    {
      ItemSwitch* result = Builder(fromCoItem).AsSwitch(itemType);
      if (result != nullptr) return *result;
      LOG(LogFatal) << "[MenuProvider] Requested item " << (int)itemType << " does not exists or not of type Switch";
      __builtin_unreachable();
    }

    ItemEditable& CoItemAsEditor(const ItemBase& fromCoItem, MenuItemType itemType)
    {
      ItemEditable* result = Builder(fromCoItem).AsEditor(itemType);
      if (result != nullptr) return *result;
      LOG(LogFatal) << "[MenuProvider] Requested item " << (int)itemType << " does not exists or not of type Editable";
      __builtin_unreachable();
    }

    template<typename T> ItemSelector<T>& ICotemAsMulti(const ItemBase& fromCoItem, MenuItemType itemType)
    {
      ItemSelector<T>* result = Builder(fromCoItem).AsMulti<T>(itemType);
      if (result != nullptr) return *result;
      LOG(LogFatal) << "[MenuProvider] Requested item " << (int)itemType << " does not exists or not of type Multi";
      __builtin_unreachable();
    }

    template<typename T> ItemSelector<T>& CoItemAsList(const ItemBase& fromCoItem, MenuItemType itemType)
    {
      ItemSelector<T>* result = Builder(fromCoItem).AsList<T>(itemType);
      if (result != nullptr) return *result;
      { LOG(LogFatal) << "[MenuProvider] Requested item " << (int)itemType << " does not exists or not of type List"; }
      __builtin_unreachable();
    }

    /*
     * Tool method
     */

    /*!
     * @brief Check high level filter after the given boolean is modifier, and revert to the original value
     * if the new filter hide all games
     * @param value New value passed by adress so that it can be reverted
     * @param setter Boolean setter in RecalboxConf that set the boolean filter
     * @param message Optional message (passing nullptr display a generic message in cas eof error)
     * @param inverted True to process the value as a inverted value when calling the setter
     */
    void CheckHighLevelFilter(bool& value, SetBoolMethod setter, const char* message, bool inverted);

    //! Check theme compatibility
    String CheckCompatibility(const Path& themePath, [[out]] bool& compatible, bool switchTheme);

    /*!
     * @brief Rebuild theme option items
     * @param item Menu Item to change content
     * @param selected Selected values
     * @param items Values
     */
    void ChangeThemeOptionSelector(ItemSelector<String>& item, const String& selected, const String::List& items);

    /*!
     * @brief Try to sort item list by their front numeric value when all items have numeric values
     * If at least one item has no numeric value, the list is sorted alphanumerically
     * @param list String list to sort
     * @return Sorted list
     */
    bool TrySortNumerically(SelectorEntry<String>::List& list);

    /*!
     * @brief Do jamma configuration reset
     * @param menu Menu object
     */
    void DoJammaReset(MenuBuilder& menu);

    /*!
     * @brief Run & select menu calibration
     */
    void RunScreenCalibration();

    /*
     * Sub menus
     */

    void SubMenuSelected(const ItemSubMenu& item, int id) final;

    /*
     * IActionTriggered implementation
     */

    void MenuActionTriggered(ItemAction& item, int id) final;

    /*
     *  ISwitchChanged implementation
     */

    void MenuSwitchChanged(const ItemSwitch& item, bool& value, int id) final;

    /*
     * IOptionListComponent<String> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<String>& item, int id, int index, const String& value) final;

    /*
     * IOptionListComponent<StorageDevices::Device> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<StorageDevices::Device>& item, int id, int index, const StorageDevices::Device& value) final;

    /*
     * ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperImageType> implementations
     */

    void MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperImageType>& item, int id, int index, const ScreenScraperEnums::ScreenScraperImageType& value) final;

    /*
     * ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperVideoType> implementations
     */

    void MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperVideoType>& item, int id, int index, const ScreenScraperEnums::ScreenScraperVideoType& value) final;

    /*
     * ISingleSelectorChanged<ScreenScraperEnums::ScreenScraperRegionPriority> implementations
     */

    void MenuSingleChanged(ItemSelectorBase<ScreenScraperEnums::ScreenScraperRegionPriority>& item, int id, int index, const ScreenScraperEnums::ScreenScraperRegionPriority& value) final;

    /*
     * ISingleSelectorChanged<Regions::GameRegions> implementations
     */

    void MenuSingleChanged(ItemSelectorBase<Regions::GameRegions>& item, int id, int index, const Regions::GameRegions& value) final;

    /*
     * ISingleSelectorChanged<Languages> implementations
     */

    void MenuSingleChanged(ItemSelectorBase<Languages>& item, int id, int index, const Languages& value) final;

    /*
     * ISingleSelectorChanged<ScraperNameOptions> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<ScraperNameOptions>& item, int id, int index, const ScraperNameOptions& value) final;

    /*
     * ISingleSelectorChanged<ScraperFactory::ScraperType> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<ScraperType>& item, int id, int index, const ScraperType& value) final;

    /*
     * ISingleSelectorChanged<ScraperType> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<ScrapingMethod>& item, int id, int index, const ScrapingMethod& value) final;

    /*
     * ISingleSelectorChanged<RecalboxConf::SoftPatching> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<RecalboxConf::SoftPatching>& item, int id, int index, const RecalboxConf::SoftPatching& value) final;

    /*
     * ISingleSelectorChanged<RecalboxConf::Relay> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<RecalboxConf::Relay>& item, int id, int index, const RecalboxConf::Relay& value) final;

    /*
     * IOptionListComponent<String> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<RecalboxConf::Screensaver>& item, int id, int index, const RecalboxConf::Screensaver& value) final;

    /*
     * IOptionListComponent<RecalboxConf::UpdateType> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<RecalboxConf::UpdateType>& item, int id, int index, const RecalboxConf::UpdateType& value) final;

    /*
     * IOptionListComponent<ThemeSpec> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<ThemeSpec>& item, int id, int index, const ThemeSpec& value) final;

    /*
     * ISingleSelectorChanged<SystemSorting> implementation
     */

    void MenuSingleChanged(ItemSelectorBase<SystemSorting>& item, int id, int index, const SystemSorting& value) final;

    /*
     * IMultiSelectorChanged<SystemData*> implementation
     */

    void MenuMultiChanged(const ItemSelectorBase<SystemData*>& item, int id, int index, const std::vector<SystemData*>& value) final;

    /*
     * IOptionListMultiComponent<String> implementation
     */

    void MenuMultiChanged(const ItemSelectorBase<String>& item, int id, int index, const std::vector<String>& value) final;

    /*
     * IOptionListMultiComponent<GameGenres> implementation
     */

    void MenuMultiChanged(const ItemSelectorBase<GameGenres>& item, int id, int index, const std::vector<GameGenres>& value) final;

    /*
     * IEditableChanged
     */

    void MenuEditableChanged(ItemEditable& item, int id, const String& newText) final;

    /*
     * Slider
     */
    void MenuSliderMoved(int id, float value) final;

    /*
     * ISingleSelectorChanged<Overclocking::Overclock>
     */
    void MenuSingleChanged(ItemSelectorBase<Overclocking::Overclock>& item, int id, int index, const Overclocking::Overclock& value) final;

    /*
     * ISingleSelectorChanged<CrtAdapterType>
     */
    void MenuSingleChanged(ItemSelectorBase<CrtAdapterType>& item, int id, int index, const CrtAdapterType& value) final;

    /*
     * ISingleSelectorChanged<ICrtInterface::HorizontalFrequency>
     */
    void MenuSingleChanged(ItemSelectorBase<ICrtInterface::HorizontalFrequency>& item, int id, int index, const ICrtInterface::HorizontalFrequency &value);

    /*
     * ISingleSelectorChanged<CrtScanlines>
     */
    void MenuSingleChanged(ItemSelectorBase<CrtScanlines>& item, int id, int index, const CrtScanlines& value) final;
};
