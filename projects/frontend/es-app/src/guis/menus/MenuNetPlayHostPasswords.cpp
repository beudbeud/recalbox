#include "guis/menus/MenuNetPlayHostPasswords.h"
#include <guis/menus/MenuNetPlayEditPasswords.h>
#include <views/ViewController.h>

MenuNetPlayHostPasswords::MenuNetPlayHostPasswords(WindowManager& window, FileData& game)
  : Menu(window, InheritableContext(), _("GAME PROTECTION"))
  , mGame(game)
{
}

void MenuNetPlayHostPasswords::BuildMenuItems()
{
  // Use password for players
  AddSwitch(_("USE PLAYER PASSWORD"), RecalboxConf::Instance().GetNetplayPasswordUseForPlayer(), (int)Components::UseForPlayers, this);
  // Use password for viewers
  AddSwitch(_("USE PLAYER PASSWORD"), RecalboxConf::Instance().GetNetplayPasswordUseForPlayer(), (int)Components::UseForViewers, this);

  // Password selection for players
  AddList<int>(_("CHOOSE PLAYER PASSWORD"), (int)Components::LastForPlayers, this, GetPasswords(), RecalboxConf::Instance().GetNetplayPasswordLastForPlayer(), 0);
  // Password selection for viewers
  AddList<int>(_("CHOOSE VIEWER-ONLY PASSWORD"), (int)Components::LastForViewers, this, GetPasswords(), RecalboxConf::Instance().GetNetplayPasswordLastForViewer(), 0);

  AddAction(_("START GAME"), _("RUN"), (int)Components::Start, true, this, String::Empty);
  AddAction(_("CANCEL"), _(""), (int)Components::Cancel, false, this, String::Empty);
  AddAction(_("EDIT PASSWORDS"), _("EDIT"), (int)Components::EditPasswords, true, this, String::Empty);
}

SelectorEntry<int>::List MenuNetPlayHostPasswords::GetPasswords()
{
  SelectorEntry<int>::List list;
  for(int i = 0; i < DefaultPasswords::sPasswordCount; i++)
  {
    String password = RecalboxConf::Instance().GetNetplayPasswords(i);
    if (password.empty()) password = DefaultPasswords::sDefaultPassword[i];
    list.push_back({ password, i });
  }
  return list;
}

void MenuNetPlayHostPasswords::MenuSwitchChanged(const ItemSwitch& item, bool& state, int id)
{
  (void)item;
  switch((Components)id)
  {
    case Components::UseForPlayers: mConf.SetNetplayPasswordUseForPlayer(state).Save(); break;
    case Components::UseForViewers: mConf.SetNetplayPasswordUseForViewer(state).Save(); break;
    case Components::LastForPlayers:
    case Components::LastForViewers:
    case Components::Start:
    case Components::Cancel:
    case Components::EditPasswords:
    default: break;
  }
}

void MenuNetPlayHostPasswords::MenuSingleChanged(ItemSelectorBase<int>& item, int id, int index, const int& value)
{
  (void)item;
  (void)index;
  switch((Components)id)
  {
    case Components::UseForPlayers:
    case Components::UseForViewers:break;
    case Components::LastForPlayers: mConf.SetNetplayPasswordLastForPlayer(value).Save(); break;
    case Components::LastForViewers: mConf.SetNetplayPasswordLastForViewer(value).Save(); break;
    case Components::Start:
    case Components::Cancel:
    case Components::EditPasswords:
    default: break;
  }
}

void MenuNetPlayHostPasswords::MenuActionTriggered(ItemAction& item, int id)
{
  (void)item;
  switch((Components)id)
  {
    case Components::Start:
    {
      GameLinkedData data(RecalboxConf::Instance().GetNetplayPort(),
                          mConf.GetNetplayPasswords(mConf.GetNetplayPasswordLastForPlayer()),
                          mConf.GetNetplayPasswords(mConf.GetNetplayPasswordLastForViewer()));
      ViewController::Instance().Launch(&mGame, data, Vector3f(), !ViewController::Instance().IsInVirtualSystem());
      break;
    }
    case Components::Cancel: Close(); break;
    case Components::EditPasswords: mWindow.pushGui(new MenuNetPlayEditPasswords(mWindow)); break;
    case Components::UseForPlayers:
    case Components::UseForViewers:
    case Components::LastForPlayers:
    case Components::LastForViewers:
    default: break;
  }
}

