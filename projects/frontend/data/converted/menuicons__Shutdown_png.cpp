// this file was auto-generated from "Shutdown.png" by res2h 

#include "../Resources.h"

const uint16_t menuicons__Shutdown_png_size = 1072;
const uint8_t menuicons__Shutdown_png_data[1072] = {
    0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a,0x00,0x00,0x00,0x0d,0x49,0x48,
    0x44,0x52,0x00,0x00,0x00,0x32,0x00,0x00,0x00,0x32,0x08,0x06,0x00,0x00,
    0x00,0x1e,0x3f,0x88,0xb1,0x00,0x00,0x03,0xf7,0x49,0x44,0x41,0x54,0x58,
    0x09,0xed,0x97,0xef,0x8b,0x0d,0x51,0x18,0xc7,0xef,0x6c,0xc0,0x2e,0x60,
    0xbb,0x2a,0xda,0xc2,0xb2,0xa4,0x60,0x91,0x0d,0x96,0x82,0xa8,0xf2,0x17,
    0x78,0x45,0x29,0xa9,0x88,0x92,0xf2,0x4e,0x95,0x57,0x2a,0x50,0x45,0x44,
    0xe5,0xdd,0x4a,0x45,0x54,0x44,0x29,0x58,0x36,0x88,0x15,0x10,0x25,0xd9,
    0x15,0xec,0x86,0xad,0xb0,0x76,0x7d,0x9e,0xc6,0x9d,0x9e,0xfb,0x34,0x33,
    0x77,0x7e,0xdd,0xab,0x72,0xb7,0xef,0x67,0xce,0x79,0xce,0x79,0xce,0xf7,
    0x99,0x33,0xb3,0x33,0xf7,0xde,0x5c,0xae,0xfa,0x57,0xbd,0x02,0xd5,0x2b,
    0x10,0x76,0x05,0x9c,0xb0,0xc9,0x34,0x73,0x83,0x83,0x83,0xe3,0x59,0x7f,
    0x03,0xb4,0x96,0x3b,0x8e,0xd3,0xa3,0x07,0xb2,0xea,0x0f,0xc9,0xca,0xc8,
    0xc7,0x47,0xbc,0x67,0x9b,0x71,0x19,0x33,0x43,0xd9,0x84,0x35,0xd9,0xd8,
    0xfc,0x7b,0x97,0xea,0x46,0xe4,0x1e,0xf0,0x1c,0xe4,0x61,0x95,0xf4,0xd3,
    0x80,0xc7,0x6e,0x98,0x98,0xc6,0x23,0xf1,0x5a,0x0a,0x6f,0x81,0xcf,0xd0,
    0x05,0xb5,0xd6,0x88,0xb1,0x3c,0x58,0xe5,0x7d,0xf2,0xd6,0xfc,0x4d,0xea,
    0xa5,0xdd,0x01,0x65,0x7b,0x01,0x15,0xd5,0xa6,0x50,0x1d,0xb4,0x81,0xd6,
    0xde,0xa2,0x24,0x02,0x26,0xa3,0x6e,0xe4,0x36,0xb9,0x5a,0x17,0x09,0xe4,
    0x8d,0x87,0x4b,0x99,0x44,0x81,0x71,0xd0,0x0e,0x56,0x1f,0x19,0x18,0xad,
    0xcb,0x12,0xe7,0xc1,0xaa,0xe8,0x8e,0x30,0xb9,0x0a,0xfc,0x74,0x9f,0xc1,
    0x71,0xda,0xaf,0x54,0xbf,0xa6,0x54,0x42,0x61,0x1e,0x63,0x39,0xd1,0x4b,
    0xc4,0x2d,0xa0,0x35,0x48,0x70,0x19,0x86,0x41,0x5c,0x3d,0x65,0xc1,0xe9,
    0x5c,0x2e,0x37,0x00,0x5a,0xcd,0x04,0x97,0xa8,0x39,0x8a,0x36,0x3b,0x61,
    0xe8,0xc0,0x05,0xb0,0xfa,0xc4,0xc0,0x3a,0xbf,0x4a,0x8c,0x97,0xbc,0x23,
    0x85,0x75,0xe4,0xae,0x87,0x1e,0xb0,0x6a,0x2b,0xe4,0x64,0xd2,0xe2,0xbe,
    0x0d,0xac,0x5e,0x33,0x30,0x25,0x17,0xf0,0xc7,0x5c,0xe4,0x8d,0x88,0x05,
    0xf9,0x33,0xe1,0x1d,0x58,0x6d,0x95,0xf9,0xd4,0xe0,0xda,0x00,0x7d,0xa0,
    0xf5,0x81,0x60,0x46,0x98,0x39,0xf3,0x79,0xb0,0x2a,0x7a,0x46,0xec,0x7a,
    0x92,0x9b,0xa0,0x1b,0xb4,0xfa,0x08,0x26,0xdb,0xdc,0xd8,0x31,0x26,0x67,
    0x40,0xab,0x9f,0x60,0x45,0x29,0x23,0x72,0xf2,0x60,0x15,0xba,0x11,0xf1,
    0x64,0x41,0x2b,0x48,0x0d,0x1a,0x4f,0xa7,0x64,0x2e,0x31,0xd8,0xc8,0x15,
    0xfa,0x4d,0xab,0x75,0x24,0x8a,0x21,0x0b,0x12,0x6d,0x44,0xbc,0x59,0x7b,
    0x18,0xb4,0x64,0x63,0x53,0x65,0x2e,0x11,0x38,0xed,0x07,0x2d,0xf9,0x00,
    0x8c,0xf4,0x5a,0x64,0x51,0x9a,0x8d,0xd4,0xb3,0xbe,0x17,0xb4,0xf6,0x25,
    0xdd,0x84,0xbc,0xa9,0xe4,0x81,0xd6,0x66,0x07,0xa2,0x9a,0xb1,0x28,0xf1,
    0x46,0xa4,0x06,0xeb,0x0f,0x81,0xd6,0x4b,0x19,0x8f,0x0d,0x0e,0x4d,0x60,
    0x35,0x37,0xaa,0x11,0x0b,0xd3,0x6e,0xa4,0x19,0x0f,0xab,0x69,0x41,0xf5,
    0x6b,0x82,0x26,0x18,0x6f,0x01,0xad,0xb7,0x8e,0xe3,0x3c,0xd2,0x03,0xe5,
    0xec,0x53,0xeb,0x01,0xfe,0xef,0x41,0x6b,0x89,0x0e,0x74,0x3f,0x6c,0x23,
    0x0b,0x74,0x22,0xfd,0xbb,0x50,0x69,0xb5,0x9b,0x82,0xf2,0x89,0x6f,0x86,
    0xdc,0x30,0x6c,0x23,0xf6,0x55,0x99,0xec,0x7f,0xd4,0xad,0x93,0xf4,0xf8,
    0xca,0x2c,0xb4,0xe7,0xe4,0x4d,0x87,0x6d,0x64,0x82,0x97,0xe5,0x76,0xbe,
    0xb8,0x4d,0x45,0x8f,0x9f,0x4c,0xb5,0x7a,0x13,0x7b,0x61,0xd8,0x6f,0xe8,
    0xa1,0x5e,0x96,0xdb,0xf9,0xe9,0x36,0xd1,0x8e,0xfc,0x8f,0x7f,0x24,0xd3,
    0x81,0x34,0xfa,0x65,0x16,0xdb,0x73,0xf2,0xa6,0xc3,0xee,0xc8,0x77,0x2f,
    0xcb,0xed,0x8c,0x71,0x9b,0x8a,0x1e,0xc7,0x9a,0x6a,0xdf,0x4c,0xec,0x85,
    0x61,0x1b,0xe9,0xf6,0xb2,0xdc,0x4e,0xa3,0xdb,0x54,0xf4,0x68,0x6b,0x76,
    0x05,0x55,0x0f,0xdb,0xc8,0x73,0xb3,0x68,0xa1,0x89,0x2b,0x11,0xda,0x9a,
    0x2f,0x62,0x17,0xe5,0x93,0x68,0x29,0x68,0x0d,0x10,0x4c,0x8a,0x6d,0x94,
    0x70,0x01,0xb5,0x1a,0x40,0x6a,0xd2,0x78,0x5a,0x1c,0x64,0x17,0x76,0x47,
    0x3a,0x58,0xf4,0x15,0x0a,0x92,0x07,0x77,0x63,0x21,0xa8,0x40,0x2b,0xb5,
    0xa4,0x66,0xa1,0x54,0x0f,0x9d,0x7b,0x10,0x5f,0x5c,0x87,0x13,0xa0,0xf5,
    0x9e,0x60,0x44,0x7c,0xa7,0x78,0x2b,0xa8,0x31,0x12,0xec,0xef,0x92,0x63,
    0xf1,0x5c,0x54,0x36,0x66,0xad,0x60,0xb5,0x4b,0xa5,0x94,0xa5,0x4b,0xc1,
    0x3d,0x60,0xb5,0x34,0x55,0x31,0xdc,0xae,0x83,0xd6,0x57,0x82,0x19,0xa9,
    0x4c,0x43,0x16,0xe3,0x3d,0x0b,0xfa,0x40,0xeb,0x6a,0xc8,0x92,0x68,0x53,
    0xb8,0xb5,0xc0,0x6f,0xd0,0x7a,0x48,0x30,0x3a,0x9a,0x43,0xf4,0x2c,0x3c,
    0xc7,0xc0,0x63,0xd0,0xea,0x27,0xb0,0x6f,0xaf,0xe8,0xa6,0x3a,0x13,0xa3,
    0xa3,0x60,0x75,0x85,0x81,0x3a,0x9d,0x97,0xa6,0x8f,0xd7,0x28,0xb8,0x06,
    0x56,0x07,0xd3,0xf8,0x16,0xad,0xc5,0xb9,0x16,0x3a,0xc1,0xaa,0x83,0x81,
    0xa9,0x45,0xc9,0x09,0x02,0x3c,0x1a,0x41,0xbc,0x68,0x8a,0x24,0x77,0x3e,
    0xdb,0x97,0x0b,0xf6,0xd3,0xa1,0x1b,0xac,0xe4,0x99,0xd9,0xce,0x60,0xd8,
    0xf7,0x36,0xdf,0xed,0xc9,0x1a,0xd8,0x09,0xdf,0xc0,0xaa,0x8b,0x81,0xd4,
    0x17,0x29,0xa8,0xf0,0x7c,0xcc,0x3f,0x80,0x9f,0x9e,0x31,0xb8,0x09,0x4a,
    0x7e,0x27,0x93,0x1c,0xd8,0x0c,0x2f,0xc0,0x4f,0x72,0xc1,0xe6,0xf9,0x9e,
    0x44,0xc0,0xa0,0x13,0x30,0x1e,0x38,0x4c,0x55,0x79,0x63,0x9d,0x27,0x61,
    0x16,0xf8,0xe9,0x07,0x83,0x1d,0x70,0x07,0x3a,0xa1,0x0f,0x44,0x75,0x1c,
    0xe6,0x80,0xfc,0xf2,0x5c,0x44,0x3b,0x1c,0xfc,0xf4,0x84,0xc1,0x0d,0x8e,
    0xe3,0xbc,0xa1,0x2d,0xaf,0xd8,0x8c,0x3c,0x98,0x27,0x69,0x07,0x20,0x2b,
    0x89,0xd7,0x71,0xcc,0x6a,0xcb,0x7b,0xf6,0x3e,0xee,0x14,0x6d,0x85,0x76,
    0x48,0xab,0x5b,0x18,0x2c,0xf3,0x29,0x51,0xd9,0x21,0x4e,0x62,0x2d,0x9c,
    0x83,0x9f,0x10,0x55,0x3f,0x48,0x3c,0x0b,0xab,0xb3,0x38,0xdb,0xd8,0xcf,
    0x48,0x58,0x51,0x4e,0x6a,0x3c,0xf3,0x2b,0x61,0x09,0xc8,0xb3,0xd4,0x40,
    0x5b,0xa8,0x31,0x40,0xff,0x1d,0xc8,0xef,0xf0,0x9b,0xb4,0xd7,0x1d,0xc7,
    0xe9,0xa5,0xad,0xaa,0x7a,0x05,0xaa,0x57,0xa0,0x7a,0x05,0xfe,0xc3,0x2b,
    0xf0,0x07,0x8b,0x53,0x10,0xfc,0xde,0x9f,0x0c,0x43,0x00,0x00,0x00,0x00,
    0x49,0x45,0x4e,0x44,0xae,0x42,0x60,0x82
};

