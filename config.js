module.exports = {
  endpoint: 'https://gitlab.com/api/v4',
  hostRules: [],
  platform: 'gitlab',
  username: 'renovate-bot',
  gitAuthor: 'Renovate Bot <renovate@recalbox.com>',
  autodiscover: false,
  allowedPostUpgradeCommands: ['./updater.py']
};
